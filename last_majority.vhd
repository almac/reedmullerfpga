library IEEE; 
use IEEE.std_logic_1164.all;  
use ieee.std_logic_arith.all ;
use ieee.std_logic_unsigned.all;

entity last_majority is 
	port (
		start: in std_logic;
		x1: in std_logic_vector( 7 downto 0);  
		x2: in std_logic_vector( 7 downto 0);  
		x3: in std_logic_vector( 7 downto 0);  
		v: in std_logic_vector( 2 downto 0);
		me: in std_logic_vector( 7 downto 0);
		
		decoded_message: out std_logic_vector(7 downto 0)
	);  
end last_majority;  

architecture rtl of last_majority is

	signal v1,v2,v3,my,vec_cobined :std_logic_vector (7 downto 0);
	signal v1_out,v2_out,v3_out :std_logic_vector (7 downto 0);
	
	function create_vector (x: std_logic) return std_logic_vector is
		begin
			if( x = '1') then
			return "11111111";
		else
			return "00000000";
		end if;
	end function;
	
	function get_majority (x: std_logic_vector) return std_logic is

		variable zeros : integer := 0;
		variable ones : integer := 0;

		begin
		for i in 0 to 7 loop
			if (x(i) = '1') then
				ones:=ones+1;
			else
				zeros:=zeros+1;
			end if;
		end loop;
	
		if zeros>ones then
			return '0';
		else
			return '1';
		end if;
	end function;
	
	begin
		process(start,me,v,v1,v2,v3,my,vec_cobined)
		
		--variable v1,v2,v3,my,vec_cobined :std_logic_vector (7 downto 0);
		--variable v1_out,v2_out,v3_out :std_logic_vector (7 downto 0);
		--variable majority:std_logic;
		
			begin
				if( start = '1') then
					v1 <= create_vector(v(2));
					v2 <= create_vector(v(1));
					v3 <= create_vector(v(0));
		
					v1_out <= x1 and v1;
					v2_out <= x2 and v2;
					v3_out <= x3 and v3;
		
					my <= v1_out xor v2_out xor v3_out;
					vec_cobined <=(my xor me);
					decoded_message <= ("0000" & get_majority(vec_cobined) & v(2 downto 0));
				end if;
			end process;
	end rtl;  