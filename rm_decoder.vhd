library IEEE; 
use IEEE.std_logic_1164.all;  
use ieee.std_logic_arith.all ;
use ieee.std_logic_unsigned.all;

	entity rm_decoder is 
		port (  
			me: in std_logic_vector(7 downto 0);
			decoded_message: out std_logic_vector(7 downto 0)  
		);  
	end rm_decoder;  

	architecture rtl of rm_decoder is

		signal x1,x2,x3: std_logic_vector(7 downto 0);
		signal decoded_message_temp: std_logic_vector(2 downto 0);
		signal majority_counter: std_logic_vector(2 downto 0) ;  
		signal start_maj1,start_maj2,start_maj3, start_last_maj: std_logic;
		
		component vector_majority is
			port(
				me: in std_logic_vector(7 downto 0) ;  
				x1: in std_logic_vector(7 downto 0) ;
				x2: in std_logic_vector(7 downto 0) ;
				start: in std_logic;
				data_ready: out std_logic;
				majority: out std_logic 
			);
		end component;
		
		component last_majority is
			port(
				start: in std_logic;
				x1: in std_logic_vector( 7 downto 0);  
				x2: in std_logic_vector( 7 downto 0);  
				x3: in std_logic_vector( 7 downto 0);  
				v: in std_logic_vector( 2 downto 0);
				me: in std_logic_vector( 7 downto 0);
		
				decoded_message: out std_logic_vector(7 downto 0)
			);
		end component;
		
		begin
			--majority counter for x3
			maj1: vector_majority
				port map (me, x1, x2, start_maj1, majority_counter(0),decoded_message_temp(0));
			--majority counter for x2
			maj2: vector_majority
				port map (me, x1, x3, start_maj1, majority_counter(1),decoded_message_temp(1));
			--majority counter for x1
			maj3: vector_majority
				port map (me, x2, x3, start_maj1, majority_counter(2),decoded_message_temp(2));
			--last majority counter
			last_maj: last_majority
				port map ( start_last_maj, x1, x2, x3, decoded_message_temp, me, decoded_message);
			
			--process for counting majorities for bites 1 to n
			process(me)
			begin
				x1<="00001111";
				x2<="00110011";
				x3<="01010101";
				
				start_maj1<='1';
				start_maj2<='1';
				start_maj3<='1';
			end process;
			
			--count majority for first bit
			process(majority_counter)
			begin
				if( majority_counter = "111") then
					start_last_maj<='1';
				end if;
			end process;			
			
		end rtl;  