library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.STD_LOGIC_ARITH.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL; 

entity rs232_rx is 
generic(
	INCLK : natural := 50000000;
	BAUDRATE : natural := 9600
);
port( 
    CLK : in std_logic; 
    RST  : in std_logic; 
    RST_out  : out std_logic; 
    RxD: in std_logic;
	 RxD_1_out : out std_logic;
	RxD_2_out : out std_logic;
RxD_3_out : out std_logic;
RxD_4_out : out std_logic;
RxD_5_out : out std_logic;
RxD_6_out : out std_logic;
RxD_7_out : out std_logic;
RxD_8_out : out std_logic;	
    BusyTxD : out std_logic; 
    DataTxD : out std_logic
	 );
end rs232_rx; 

architecture Behavioral of rs232_rx is 
	 
	 -- synchro
	 constant CNT_WIDTH : natural := 4;
	 constant DIVVALUE : natural := 10;
	 constant PULSVALUE : natural := 5;
	 
	 signal takt_counter: integer;
	 --receiver
    signal temp : std_logic_vector(7 downto 0); 
    signal counter: integer; 
    signal Parallel1:std_logic_vector(7 downto 0); 
	 signal startBit: std_logic;
	 --decoder
	 signal dataTxD_r: std_logic_vector(7 downto 0);
	 --transmiter
	 signal dataBuffer: std_logic_vector(9 downto 0);
	 
	 type stateType is (IDLE, START_BIT ,RECEIVE, DECODE, TRANSMIT); 
    signal currentState, nextState : stateType; 
	
	component rm_decoder is
			port(
				me: in std_logic_vector(7 downto 0);
				decoded_message: out std_logic_vector(7 downto 0)  
			);
		end component;
		
	  begin 
		
		rm_dec: rm_decoder
			port map ( Parallel1, dataTxD_r );
	 -- reset out
	  RST_out <= RST;
	  
 process(currentState, RST, CLK, StartBit, dataTxD_r ) 
        begin 
            if(CLK='1' and CLK'event) then 
                if (RST='1') then 
                    temp<="00000000"; 
                    counter<=0; 
						  takt_counter <= 0;
                    currentState <= IDLE; 
                else 
                    case currentState is 
                        when IDLE => 
									 temp<="00000000"; 
									 takt_counter <= 0;
                            busyTxD <= '0'; 
                            DataTxD <= '1'; 
                            if(RxD = '0') then
										  startBit <= '0';
                                currentState <= START_BIT;
                            else 
                                currentState <= IDLE; 
                            end if; 
							   when START_BIT =>
									 if(takt_counter < 5208) then   --tu zmienic 10 na 5208 dla 50 MHz.
										  takt_counter <= takt_counter + 1;
										  currentState <= START_BIT;
									 else
										  takt_counter <= 0; 
										  startBit <= '0';
										  currentState <= RECEIVE;
									 end if;
                        when RECEIVE =>
									 --takt_counter <= takt_counter + 1;	
									 if (takt_counter < 5208) then
										  takt_counter <= takt_counter + 1;	
									     if (counter < 8 and takt_counter = 2604) then --zmienic potem 5 na 2604 zebey bylo dla 50MHz
												temp(counter)<=RxD; 
												counter<=counter+1; 
										  elsif(counter = 8) then
												Parallel1 <= temp; 
												counter <= 0; 
												takt_counter <= 0;
												currentState <= DECODE;
												
									 RxD_2_out <= temp(6);
									 RxD_3_out <= temp(5);
									 RxD_4_out <= temp(4);
									 RxD_5_out <= temp(3);
									 RxD_6_out <= temp(2);
									 RxD_7_out <= temp(1);
									 RxD_8_out <= temp(0);
										  else 
												currentState <= RECEIVE;
										  end if;
									 else 
										  takt_counter <= 0;
                            end if; 
                        when DECODE => 
									RxD_1_out <= RxD;
                            --tu powinno byc dekodowanie 
                            --dataBuffer <= "1111100000";--'1'&Parallel1&'1';
									 dataBuffer<='1'& dataTxD_r &'0';
                            counter <= 0; 
									 
                            currentState <= TRANSMIT;
                        when TRANSMIT => 
									 BusyTxD <='1'; 
									  
                            if(takt_counter < 5208) --zmienic potem 10 na 5208
                            then  
										  takt_counter <= takt_counter + 1;
										  if(counter < 10 and takt_counter = 0)
										  then 
												DataTxD <= dataBuffer(counter);
												counter<=counter + 1;	
										  elsif(counter = 10 and takt_counter = 5207) --zmienic 10 na 5207
										  then 
												counter <=0; 
												currentState <=IDLE; 
										  else 
												currentState <= TRANSMIT;
										  end if;
                            else
										  
										  takt_counter <= 0; 
                            end if; 
                    end case;
					end if; 
            end if; 
        end process; 
end Behavioral;