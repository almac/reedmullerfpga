-- Copyright (C) 1991-2011 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 11.0 Build 208 07/03/2011 Service Pack 1 SJ Web Edition"

-- DATE "05/19/2014 14:52:29"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	rs232_rx IS
    PORT (
	RST_out : OUT std_logic;
	RxD_1_out : OUT std_logic;
	RxD_2_out : OUT std_logic;
	RxD_3_out : OUT std_logic;
	RxD_4_out : OUT std_logic;
	RxD_5_out : OUT std_logic;
	RxD_6_out : OUT std_logic;
	RxD_7_out : OUT std_logic;
	RxD_8_out : OUT std_logic;
	BusyTxD : OUT std_logic;
	DataTxD : OUT std_logic;
	CLK : IN std_logic;
	RST : IN std_logic;
	RxD : IN std_logic
	);
END rs232_rx;

-- Design Ports Information
-- RST_out	=>  Location: PIN_AD12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_1_out	=>  Location: PIN_AE13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_2_out	=>  Location: PIN_AF13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_3_out	=>  Location: PIN_AE15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_4_out	=>  Location: PIN_AD15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_5_out	=>  Location: PIN_AC14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_6_out	=>  Location: PIN_AA13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_7_out	=>  Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RxD_8_out	=>  Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- BusyTxD	=>  Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DataTxD	=>  Location: PIN_B25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- RST	=>  Location: PIN_V2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- RxD	=>  Location: PIN_C25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLK	=>  Location: PIN_N2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF rs232_rx IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_RST_out : std_logic;
SIGNAL ww_RxD_1_out : std_logic;
SIGNAL ww_RxD_2_out : std_logic;
SIGNAL ww_RxD_3_out : std_logic;
SIGNAL ww_RxD_4_out : std_logic;
SIGNAL ww_RxD_5_out : std_logic;
SIGNAL ww_RxD_6_out : std_logic;
SIGNAL ww_RxD_7_out : std_logic;
SIGNAL ww_RxD_8_out : std_logic;
SIGNAL ww_BusyTxD : std_logic;
SIGNAL ww_DataTxD : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_RST : std_logic;
SIGNAL ww_RxD : std_logic;
SIGNAL \CLK~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \currentState.TRANSMIT~regout\ : std_logic;
SIGNAL \Add0~2_combout\ : std_logic;
SIGNAL \Add0~4_combout\ : std_logic;
SIGNAL \Add0~10_combout\ : std_logic;
SIGNAL \Add0~12_combout\ : std_logic;
SIGNAL \Add0~14_combout\ : std_logic;
SIGNAL \Add0~32_combout\ : std_logic;
SIGNAL \Add0~42_combout\ : std_logic;
SIGNAL \Add0~44_combout\ : std_logic;
SIGNAL \Add0~62\ : std_logic;
SIGNAL \Add0~90_combout\ : std_logic;
SIGNAL \Add1~15_combout\ : std_logic;
SIGNAL \Add1~36_combout\ : std_logic;
SIGNAL \Add1~39_combout\ : std_logic;
SIGNAL \Add1~42_combout\ : std_logic;
SIGNAL \Add1~51_combout\ : std_logic;
SIGNAL \Add1~66_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~4_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~7\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~8_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~6_combout\ : std_logic;
SIGNAL \process_0~3_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \RxD_2_out~0_combout\ : std_logic;
SIGNAL \process_0~8_combout\ : std_logic;
SIGNAL \process_0~12_combout\ : std_logic;
SIGNAL \process_0~13_combout\ : std_logic;
SIGNAL \Add0~67_combout\ : std_logic;
SIGNAL \Add0~73_combout\ : std_logic;
SIGNAL \Add0~92_combout\ : std_logic;
SIGNAL \Add1~17_combout\ : std_logic;
SIGNAL \Add1~38_combout\ : std_logic;
SIGNAL \Add1~41_combout\ : std_logic;
SIGNAL \Add1~44_combout\ : std_logic;
SIGNAL \Add1~53_combout\ : std_logic;
SIGNAL \Selector46~0_combout\ : std_logic;
SIGNAL \rm_dec|maj1|scalar~3_combout\ : std_logic;
SIGNAL \rm_dec|maj2|Add2~0_combout\ : std_logic;
SIGNAL \rm_dec|maj3|scalar~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|ones~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add9~1_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add8~0_combout\ : std_logic;
SIGNAL \RST~combout\ : std_logic;
SIGNAL \CLK~combout\ : std_logic;
SIGNAL \CLK~clkctrl_outclk\ : std_logic;
SIGNAL \RxD~combout\ : std_logic;
SIGNAL \counter[31]~0_combout\ : std_logic;
SIGNAL \Add1~6\ : std_logic;
SIGNAL \Add1~7_combout\ : std_logic;
SIGNAL \Add1~90_combout\ : std_logic;
SIGNAL \takt_counter[31]~1_combout\ : std_logic;
SIGNAL \Add1~87_combout\ : std_logic;
SIGNAL \Add1~89_combout\ : std_logic;
SIGNAL \Equal1~6_combout\ : std_logic;
SIGNAL \Equal1~7_combout\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \Add1~18_combout\ : std_logic;
SIGNAL \Add1~20_combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \Add1~54_combout\ : std_logic;
SIGNAL \Add1~56_combout\ : std_logic;
SIGNAL \Add1~48_combout\ : std_logic;
SIGNAL \Add1~50_combout\ : std_logic;
SIGNAL \Equal1~3_combout\ : std_logic;
SIGNAL \Equal1~4_combout\ : std_logic;
SIGNAL \Equal1~11_combout\ : std_logic;
SIGNAL \takt_counter[31]~2_combout\ : std_logic;
SIGNAL \Selector44~0_combout\ : std_logic;
SIGNAL \currentState.RECEIVE~regout\ : std_logic;
SIGNAL \Selector43~0_combout\ : std_logic;
SIGNAL \currentState.START_BIT~regout\ : std_logic;
SIGNAL \takt_counter[31]~0_combout\ : std_logic;
SIGNAL \takt_counter[31]~3_combout\ : std_logic;
SIGNAL \Add0~82_combout\ : std_logic;
SIGNAL \Add0~95_combout\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \Add0~93_combout\ : std_logic;
SIGNAL \Add0~1\ : std_logic;
SIGNAL \Add0~3\ : std_logic;
SIGNAL \Add0~5\ : std_logic;
SIGNAL \Add0~6_combout\ : std_logic;
SIGNAL \Add0~80_combout\ : std_logic;
SIGNAL \Add0~7\ : std_logic;
SIGNAL \Add0~8_combout\ : std_logic;
SIGNAL \Add0~81_combout\ : std_logic;
SIGNAL \Add0~9\ : std_logic;
SIGNAL \Add0~11\ : std_logic;
SIGNAL \Add0~13\ : std_logic;
SIGNAL \Add0~15\ : std_logic;
SIGNAL \Add0~16_combout\ : std_logic;
SIGNAL \Add0~86_combout\ : std_logic;
SIGNAL \Add0~17\ : std_logic;
SIGNAL \Add0~19\ : std_logic;
SIGNAL \Add0~21\ : std_logic;
SIGNAL \Add0~22_combout\ : std_logic;
SIGNAL \Add0~88_combout\ : std_logic;
SIGNAL \Add0~23\ : std_logic;
SIGNAL \Add0~24_combout\ : std_logic;
SIGNAL \Add0~89_combout\ : std_logic;
SIGNAL \Add0~85_combout\ : std_logic;
SIGNAL \Add0~18_combout\ : std_logic;
SIGNAL \Add0~84_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \Add0~83_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \Add0~25\ : std_logic;
SIGNAL \Add0~26_combout\ : std_logic;
SIGNAL \Add0~64_combout\ : std_logic;
SIGNAL \Add0~27\ : std_logic;
SIGNAL \Add0~28_combout\ : std_logic;
SIGNAL \Add0~65_combout\ : std_logic;
SIGNAL \Add0~29\ : std_logic;
SIGNAL \Add0~30_combout\ : std_logic;
SIGNAL \Add0~66_combout\ : std_logic;
SIGNAL \Add0~31\ : std_logic;
SIGNAL \Add0~33\ : std_logic;
SIGNAL \Add0~34_combout\ : std_logic;
SIGNAL \Add0~68_combout\ : std_logic;
SIGNAL \Add0~35\ : std_logic;
SIGNAL \Add0~36_combout\ : std_logic;
SIGNAL \Add0~69_combout\ : std_logic;
SIGNAL \Add0~37\ : std_logic;
SIGNAL \Add0~38_combout\ : std_logic;
SIGNAL \Add0~70_combout\ : std_logic;
SIGNAL \Add0~39\ : std_logic;
SIGNAL \Add0~40_combout\ : std_logic;
SIGNAL \Add0~71_combout\ : std_logic;
SIGNAL \Add0~41\ : std_logic;
SIGNAL \Add0~43\ : std_logic;
SIGNAL \Add0~45\ : std_logic;
SIGNAL \Add0~47\ : std_logic;
SIGNAL \Add0~48_combout\ : std_logic;
SIGNAL \Add0~75_combout\ : std_logic;
SIGNAL \Add0~49\ : std_logic;
SIGNAL \Add0~51\ : std_logic;
SIGNAL \Add0~52_combout\ : std_logic;
SIGNAL \Add0~77_combout\ : std_logic;
SIGNAL \Add0~53\ : std_logic;
SIGNAL \Add0~54_combout\ : std_logic;
SIGNAL \Add0~78_combout\ : std_logic;
SIGNAL \Add0~55\ : std_logic;
SIGNAL \Add0~56_combout\ : std_logic;
SIGNAL \Add0~79_combout\ : std_logic;
SIGNAL \Add0~57\ : std_logic;
SIGNAL \Add0~59\ : std_logic;
SIGNAL \Add0~61_combout\ : std_logic;
SIGNAL \Add0~63_combout\ : std_logic;
SIGNAL \Add0~58_combout\ : std_logic;
SIGNAL \Add0~60_combout\ : std_logic;
SIGNAL \Add0~72_combout\ : std_logic;
SIGNAL \Add0~46_combout\ : std_logic;
SIGNAL \Add0~74_combout\ : std_logic;
SIGNAL \process_0~5_combout\ : std_logic;
SIGNAL \process_0~4_combout\ : std_logic;
SIGNAL \Add0~50_combout\ : std_logic;
SIGNAL \Add0~76_combout\ : std_logic;
SIGNAL \process_0~6_combout\ : std_logic;
SIGNAL \process_0~7_combout\ : std_logic;
SIGNAL \process_0~22_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \currentState~12_combout\ : std_logic;
SIGNAL \Add0~94_combout\ : std_logic;
SIGNAL \process_0~18_combout\ : std_logic;
SIGNAL \Add0~20_combout\ : std_logic;
SIGNAL \Add0~87_combout\ : std_logic;
SIGNAL \process_0~19_combout\ : std_logic;
SIGNAL \process_0~15_combout\ : std_logic;
SIGNAL \process_0~20_combout\ : std_logic;
SIGNAL \process_0~21_combout\ : std_logic;
SIGNAL \currentState~13_combout\ : std_logic;
SIGNAL \currentState.IDLE~regout\ : std_logic;
SIGNAL \LessThan2~0_combout\ : std_logic;
SIGNAL \Add1~60_combout\ : std_logic;
SIGNAL \Add1~62_combout\ : std_logic;
SIGNAL \Add1~68_combout\ : std_logic;
SIGNAL \Equal1~5_combout\ : std_logic;
SIGNAL \Equal1~8_combout\ : std_logic;
SIGNAL \counter[31]~2_combout\ : std_logic;
SIGNAL \counter[31]~3_combout\ : std_logic;
SIGNAL \counter[31]~4_combout\ : std_logic;
SIGNAL \counter[31]~5_combout\ : std_logic;
SIGNAL \counter[31]~6_combout\ : std_logic;
SIGNAL \Add1~8\ : std_logic;
SIGNAL \Add1~9_combout\ : std_logic;
SIGNAL \Add1~11_combout\ : std_logic;
SIGNAL \Add1~10\ : std_logic;
SIGNAL \Add1~12_combout\ : std_logic;
SIGNAL \Add1~14_combout\ : std_logic;
SIGNAL \Add1~13\ : std_logic;
SIGNAL \Add1~16\ : std_logic;
SIGNAL \Add1~19\ : std_logic;
SIGNAL \Add1~21_combout\ : std_logic;
SIGNAL \Add1~23_combout\ : std_logic;
SIGNAL \Add1~22\ : std_logic;
SIGNAL \Add1~24_combout\ : std_logic;
SIGNAL \Add1~26_combout\ : std_logic;
SIGNAL \Add1~25\ : std_logic;
SIGNAL \Add1~27_combout\ : std_logic;
SIGNAL \Add1~29_combout\ : std_logic;
SIGNAL \Add1~28\ : std_logic;
SIGNAL \Add1~30_combout\ : std_logic;
SIGNAL \Add1~32_combout\ : std_logic;
SIGNAL \Add1~31\ : std_logic;
SIGNAL \Add1~33_combout\ : std_logic;
SIGNAL \Add1~35_combout\ : std_logic;
SIGNAL \Add1~34\ : std_logic;
SIGNAL \Add1~37\ : std_logic;
SIGNAL \Add1~40\ : std_logic;
SIGNAL \Add1~43\ : std_logic;
SIGNAL \Add1~45_combout\ : std_logic;
SIGNAL \Add1~47_combout\ : std_logic;
SIGNAL \Add1~46\ : std_logic;
SIGNAL \Add1~49\ : std_logic;
SIGNAL \Add1~52\ : std_logic;
SIGNAL \Add1~55\ : std_logic;
SIGNAL \Add1~57_combout\ : std_logic;
SIGNAL \Add1~59_combout\ : std_logic;
SIGNAL \Add1~58\ : std_logic;
SIGNAL \Add1~61\ : std_logic;
SIGNAL \Add1~63_combout\ : std_logic;
SIGNAL \Add1~65_combout\ : std_logic;
SIGNAL \Add1~64\ : std_logic;
SIGNAL \Add1~67\ : std_logic;
SIGNAL \Add1~69_combout\ : std_logic;
SIGNAL \Add1~71_combout\ : std_logic;
SIGNAL \Add1~70\ : std_logic;
SIGNAL \Add1~72_combout\ : std_logic;
SIGNAL \Add1~74_combout\ : std_logic;
SIGNAL \Add1~73\ : std_logic;
SIGNAL \Add1~75_combout\ : std_logic;
SIGNAL \Add1~77_combout\ : std_logic;
SIGNAL \Add1~76\ : std_logic;
SIGNAL \Add1~78_combout\ : std_logic;
SIGNAL \Add1~80_combout\ : std_logic;
SIGNAL \Add1~79\ : std_logic;
SIGNAL \Add1~81_combout\ : std_logic;
SIGNAL \Add1~83_combout\ : std_logic;
SIGNAL \Add1~82\ : std_logic;
SIGNAL \Add1~84_combout\ : std_logic;
SIGNAL \Add1~86_combout\ : std_logic;
SIGNAL \Add1~85\ : std_logic;
SIGNAL \Add1~88\ : std_logic;
SIGNAL \Add1~91_combout\ : std_logic;
SIGNAL \Add1~93_combout\ : std_logic;
SIGNAL \process_0~2_combout\ : std_logic;
SIGNAL \process_0~9_combout\ : std_logic;
SIGNAL \process_0~10_combout\ : std_logic;
SIGNAL \process_0~11_combout\ : std_logic;
SIGNAL \LessThan1~0_combout\ : std_logic;
SIGNAL \process_0~14_combout\ : std_logic;
SIGNAL \process_0~16_combout\ : std_logic;
SIGNAL \process_0~17_combout\ : std_logic;
SIGNAL \counter[31]~1_combout\ : std_logic;
SIGNAL \Add1~0_combout\ : std_logic;
SIGNAL \Add1~2_combout\ : std_logic;
SIGNAL \Add1~1\ : std_logic;
SIGNAL \Add1~3_combout\ : std_logic;
SIGNAL \Add1~95_combout\ : std_logic;
SIGNAL \Add1~4\ : std_logic;
SIGNAL \Add1~5_combout\ : std_logic;
SIGNAL \Add1~94_combout\ : std_logic;
SIGNAL \Equal1~9_combout\ : std_logic;
SIGNAL \Equal1~10_combout\ : std_logic;
SIGNAL \RxD_2_out~1_combout\ : std_logic;
SIGNAL \currentState.DECODE~regout\ : std_logic;
SIGNAL \RxD_1_out~0_combout\ : std_logic;
SIGNAL \RxD_1_out~reg0_regout\ : std_logic;
SIGNAL \Selector1~1_combout\ : std_logic;
SIGNAL \Selector1~2_combout\ : std_logic;
SIGNAL \Selector1~3_combout\ : std_logic;
SIGNAL \Selector3~1_combout\ : std_logic;
SIGNAL \Selector1~0_combout\ : std_logic;
SIGNAL \Selector1~4_combout\ : std_logic;
SIGNAL \RxD_2_out~reg0feeder_combout\ : std_logic;
SIGNAL \RxD_2_out~reg0_regout\ : std_logic;
SIGNAL \Selector2~2_combout\ : std_logic;
SIGNAL \Selector2~0_combout\ : std_logic;
SIGNAL \Selector2~1_combout\ : std_logic;
SIGNAL \Selector2~3_combout\ : std_logic;
SIGNAL \RxD_3_out~reg0_regout\ : std_logic;
SIGNAL \Selector1~5_combout\ : std_logic;
SIGNAL \Selector3~2_combout\ : std_logic;
SIGNAL \Selector3~3_combout\ : std_logic;
SIGNAL \RxD_4_out~reg0feeder_combout\ : std_logic;
SIGNAL \RxD_4_out~reg0_regout\ : std_logic;
SIGNAL \Selector4~0_combout\ : std_logic;
SIGNAL \Selector4~1_combout\ : std_logic;
SIGNAL \Selector4~2_combout\ : std_logic;
SIGNAL \RxD_5_out~reg0feeder_combout\ : std_logic;
SIGNAL \RxD_5_out~reg0_regout\ : std_logic;
SIGNAL \Selector5~0_combout\ : std_logic;
SIGNAL \Selector5~1_combout\ : std_logic;
SIGNAL \Selector5~2_combout\ : std_logic;
SIGNAL \RxD_6_out~reg0feeder_combout\ : std_logic;
SIGNAL \RxD_6_out~reg0_regout\ : std_logic;
SIGNAL \Selector6~0_combout\ : std_logic;
SIGNAL \Selector6~1_combout\ : std_logic;
SIGNAL \Selector6~2_combout\ : std_logic;
SIGNAL \RxD_7_out~reg0_regout\ : std_logic;
SIGNAL \Selector7~0_combout\ : std_logic;
SIGNAL \Selector7~1_combout\ : std_logic;
SIGNAL \Selector7~2_combout\ : std_logic;
SIGNAL \RxD_8_out~reg0_regout\ : std_logic;
SIGNAL \Selector40~0_combout\ : std_logic;
SIGNAL \BusyTxD~reg0_regout\ : std_logic;
SIGNAL \rm_dec|maj3|Add3~0_combout\ : std_logic;
SIGNAL \rm_dec|maj3|scalar~1_combout\ : std_logic;
SIGNAL \rm_dec|maj3|Add2~0_combout\ : std_logic;
SIGNAL \rm_dec|maj3|LessThan0~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add2~3_combout\ : std_logic;
SIGNAL \Selector0~0_combout\ : std_logic;
SIGNAL \Selector0~1_combout\ : std_logic;
SIGNAL \Selector0~2_combout\ : std_logic;
SIGNAL \rm_dec|maj2|Add3~0_combout\ : std_logic;
SIGNAL \rm_dec|maj2|scalar~1_combout\ : std_logic;
SIGNAL \rm_dec|maj2|scalar~0_combout\ : std_logic;
SIGNAL \rm_dec|maj2|LessThan0~0_combout\ : std_logic;
SIGNAL \rm_dec|maj1|scalar~2_combout\ : std_logic;
SIGNAL \rm_dec|maj1|scalar~1_combout\ : std_logic;
SIGNAL \rm_dec|maj1|scalar~0_combout\ : std_logic;
SIGNAL \rm_dec|maj1|LessThan0~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~2_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~1_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add2~2_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~4_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~3_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add6~1_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add8~1_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add6~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add8~2_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|vec_cobined~5_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~1_cout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~3\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~5\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~7\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~8_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add7~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add3~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add9~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~1_cout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~3\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~5\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~6_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add11~2_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~4_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|Add10~2_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|LessThan0~0_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|LessThan0~1_combout\ : std_logic;
SIGNAL \rm_dec|last_maj|LessThan0~2_combout\ : std_logic;
SIGNAL \Selector41~0_combout\ : std_logic;
SIGNAL \Selector41~1_combout\ : std_logic;
SIGNAL \Selector41~2_combout\ : std_logic;
SIGNAL \Selector3~0_combout\ : std_logic;
SIGNAL \Selector41~3_combout\ : std_logic;
SIGNAL \Selector41~4_combout\ : std_logic;
SIGNAL \Selector41~5_combout\ : std_logic;
SIGNAL \DataTxD~reg0_regout\ : std_logic;
SIGNAL Parallel1 : std_logic_vector(7 DOWNTO 0);
SIGNAL counter : std_logic_vector(31 DOWNTO 0);
SIGNAL dataBuffer : std_logic_vector(9 DOWNTO 0);
SIGNAL takt_counter : std_logic_vector(31 DOWNTO 0);
SIGNAL temp : std_logic_vector(7 DOWNTO 0);
SIGNAL \ALT_INV_RxD_1_out~0_combout\ : std_logic;
SIGNAL \ALT_INV_RST~combout\ : std_logic;

BEGIN

RST_out <= ww_RST_out;
RxD_1_out <= ww_RxD_1_out;
RxD_2_out <= ww_RxD_2_out;
RxD_3_out <= ww_RxD_3_out;
RxD_4_out <= ww_RxD_4_out;
RxD_5_out <= ww_RxD_5_out;
RxD_6_out <= ww_RxD_6_out;
RxD_7_out <= ww_RxD_7_out;
RxD_8_out <= ww_RxD_8_out;
BusyTxD <= ww_BusyTxD;
DataTxD <= ww_DataTxD;
ww_CLK <= CLK;
ww_RST <= RST;
ww_RxD <= RxD;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLK~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLK~combout\);
\ALT_INV_RxD_1_out~0_combout\ <= NOT \RxD_1_out~0_combout\;
\ALT_INV_RST~combout\ <= NOT \RST~combout\;

-- Location: LCFF_X44_Y25_N11
\currentState.TRANSMIT\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector46~0_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \currentState.TRANSMIT~regout\);

-- Location: LCCOMB_X40_Y26_N2
\Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~2_combout\ = (takt_counter(1) & (!\Add0~1\)) # (!takt_counter(1) & ((\Add0~1\) # (GND)))
-- \Add0~3\ = CARRY((!\Add0~1\) # (!takt_counter(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(1),
	datad => VCC,
	cin => \Add0~1\,
	combout => \Add0~2_combout\,
	cout => \Add0~3\);

-- Location: LCCOMB_X40_Y26_N4
\Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~4_combout\ = (takt_counter(2) & (\Add0~3\ $ (GND))) # (!takt_counter(2) & (!\Add0~3\ & VCC))
-- \Add0~5\ = CARRY((takt_counter(2) & !\Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(2),
	datad => VCC,
	cin => \Add0~3\,
	combout => \Add0~4_combout\,
	cout => \Add0~5\);

-- Location: LCCOMB_X40_Y26_N10
\Add0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~10_combout\ = (takt_counter(5) & (!\Add0~9\)) # (!takt_counter(5) & ((\Add0~9\) # (GND)))
-- \Add0~11\ = CARRY((!\Add0~9\) # (!takt_counter(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(5),
	datad => VCC,
	cin => \Add0~9\,
	combout => \Add0~10_combout\,
	cout => \Add0~11\);

-- Location: LCCOMB_X40_Y26_N12
\Add0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~12_combout\ = (takt_counter(6) & (\Add0~11\ $ (GND))) # (!takt_counter(6) & (!\Add0~11\ & VCC))
-- \Add0~13\ = CARRY((takt_counter(6) & !\Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(6),
	datad => VCC,
	cin => \Add0~11\,
	combout => \Add0~12_combout\,
	cout => \Add0~13\);

-- Location: LCCOMB_X40_Y26_N14
\Add0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~14_combout\ = (takt_counter(7) & (!\Add0~13\)) # (!takt_counter(7) & ((\Add0~13\) # (GND)))
-- \Add0~15\ = CARRY((!\Add0~13\) # (!takt_counter(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(7),
	datad => VCC,
	cin => \Add0~13\,
	combout => \Add0~14_combout\,
	cout => \Add0~15\);

-- Location: LCCOMB_X40_Y25_N0
\Add0~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~32_combout\ = (takt_counter(16) & (\Add0~31\ $ (GND))) # (!takt_counter(16) & (!\Add0~31\ & VCC))
-- \Add0~33\ = CARRY((takt_counter(16) & !\Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(16),
	datad => VCC,
	cin => \Add0~31\,
	combout => \Add0~32_combout\,
	cout => \Add0~33\);

-- Location: LCCOMB_X40_Y25_N10
\Add0~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~42_combout\ = (takt_counter(21) & (!\Add0~41\)) # (!takt_counter(21) & ((\Add0~41\) # (GND)))
-- \Add0~43\ = CARRY((!\Add0~41\) # (!takt_counter(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(21),
	datad => VCC,
	cin => \Add0~41\,
	combout => \Add0~42_combout\,
	cout => \Add0~43\);

-- Location: LCCOMB_X40_Y25_N12
\Add0~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~44_combout\ = (takt_counter(22) & (\Add0~43\ $ (GND))) # (!takt_counter(22) & (!\Add0~43\ & VCC))
-- \Add0~45\ = CARRY((takt_counter(22) & !\Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(22),
	datad => VCC,
	cin => \Add0~43\,
	combout => \Add0~44_combout\,
	cout => \Add0~45\);

-- Location: LCCOMB_X40_Y25_N28
\Add0~61\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~61_combout\ = (takt_counter(30) & (\Add0~59\ $ (GND))) # (!takt_counter(30) & (!\Add0~59\ & VCC))
-- \Add0~62\ = CARRY((takt_counter(30) & !\Add0~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(30),
	datad => VCC,
	cin => \Add0~59\,
	combout => \Add0~61_combout\,
	cout => \Add0~62\);

-- Location: LCCOMB_X40_Y25_N30
\Add0~90\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~90_combout\ = \Add0~62\ $ (!takt_counter(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => takt_counter(31),
	cin => \Add0~62\,
	combout => \Add0~90_combout\);

-- Location: LCCOMB_X41_Y25_N12
\Add1~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~15_combout\ = (counter(6) & (\Add1~13\ $ (GND))) # (!counter(6) & (!\Add1~13\ & VCC))
-- \Add1~16\ = CARRY((counter(6) & !\Add1~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(6),
	datad => VCC,
	cin => \Add1~13\,
	combout => \Add1~15_combout\,
	cout => \Add1~16\);

-- Location: LCCOMB_X41_Y25_N26
\Add1~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~36_combout\ = (counter(13) & (!\Add1~34\)) # (!counter(13) & ((\Add1~34\) # (GND)))
-- \Add1~37\ = CARRY((!\Add1~34\) # (!counter(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(13),
	datad => VCC,
	cin => \Add1~34\,
	combout => \Add1~36_combout\,
	cout => \Add1~37\);

-- Location: LCCOMB_X41_Y25_N28
\Add1~39\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~39_combout\ = (counter(14) & (\Add1~37\ $ (GND))) # (!counter(14) & (!\Add1~37\ & VCC))
-- \Add1~40\ = CARRY((counter(14) & !\Add1~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(14),
	datad => VCC,
	cin => \Add1~37\,
	combout => \Add1~39_combout\,
	cout => \Add1~40\);

-- Location: LCCOMB_X41_Y25_N30
\Add1~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~42_combout\ = (counter(15) & (!\Add1~40\)) # (!counter(15) & ((\Add1~40\) # (GND)))
-- \Add1~43\ = CARRY((!\Add1~40\) # (!counter(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(15),
	datad => VCC,
	cin => \Add1~40\,
	combout => \Add1~42_combout\,
	cout => \Add1~43\);

-- Location: LCCOMB_X41_Y24_N4
\Add1~51\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~51_combout\ = (counter(18) & (\Add1~49\ $ (GND))) # (!counter(18) & (!\Add1~49\ & VCC))
-- \Add1~52\ = CARRY((counter(18) & !\Add1~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(18),
	datad => VCC,
	cin => \Add1~49\,
	combout => \Add1~51_combout\,
	cout => \Add1~52\);

-- Location: LCCOMB_X41_Y24_N14
\Add1~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~66_combout\ = (counter(23) & (!\Add1~64\)) # (!counter(23) & ((\Add1~64\) # (GND)))
-- \Add1~67\ = CARRY((!\Add1~64\) # (!counter(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(23),
	datad => VCC,
	cin => \Add1~64\,
	combout => \Add1~66_combout\,
	cout => \Add1~67\);

-- Location: LCCOMB_X47_Y17_N16
\rm_dec|last_maj|Add11~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add11~4_combout\ = (\rm_dec|last_maj|Add9~1_combout\ & (\rm_dec|last_maj|Add11~3\ $ (GND))) # (!\rm_dec|last_maj|Add9~1_combout\ & (!\rm_dec|last_maj|Add11~3\ & VCC))
-- \rm_dec|last_maj|Add11~5\ = CARRY((\rm_dec|last_maj|Add9~1_combout\ & !\rm_dec|last_maj|Add11~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add9~1_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add11~3\,
	combout => \rm_dec|last_maj|Add11~4_combout\,
	cout => \rm_dec|last_maj|Add11~5\);

-- Location: LCCOMB_X47_Y17_N18
\rm_dec|last_maj|Add11~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add11~6_combout\ = (\rm_dec|last_maj|Add9~0_combout\ & (!\rm_dec|last_maj|Add11~5\)) # (!\rm_dec|last_maj|Add9~0_combout\ & ((\rm_dec|last_maj|Add11~5\) # (GND)))
-- \rm_dec|last_maj|Add11~7\ = CARRY((!\rm_dec|last_maj|Add11~5\) # (!\rm_dec|last_maj|Add9~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \rm_dec|last_maj|Add9~0_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add11~5\,
	combout => \rm_dec|last_maj|Add11~6_combout\,
	cout => \rm_dec|last_maj|Add11~7\);

-- Location: LCCOMB_X47_Y17_N20
\rm_dec|last_maj|Add11~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add11~8_combout\ = !\rm_dec|last_maj|Add11~7\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \rm_dec|last_maj|Add11~7\,
	combout => \rm_dec|last_maj|Add11~8_combout\);

-- Location: LCCOMB_X47_Y17_N6
\rm_dec|last_maj|Add10~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add10~6_combout\ = (\rm_dec|last_maj|Add8~1_combout\ & (!\rm_dec|last_maj|Add10~5\)) # (!\rm_dec|last_maj|Add8~1_combout\ & ((\rm_dec|last_maj|Add10~5\) # (GND)))
-- \rm_dec|last_maj|Add10~7\ = CARRY((!\rm_dec|last_maj|Add10~5\) # (!\rm_dec|last_maj|Add8~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \rm_dec|last_maj|Add8~1_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add10~5\,
	combout => \rm_dec|last_maj|Add10~6_combout\,
	cout => \rm_dec|last_maj|Add10~7\);

-- Location: LCFF_X42_Y26_N13
\takt_counter[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~67_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(16));

-- Location: LCCOMB_X42_Y26_N30
\process_0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~3_combout\ = (!takt_counter(13) & (!takt_counter(15) & (!takt_counter(14) & !takt_counter(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(13),
	datab => takt_counter(15),
	datac => takt_counter(14),
	datad => takt_counter(16),
	combout => \process_0~3_combout\);

-- Location: LCFF_X42_Y26_N25
\takt_counter[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~73_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(22));

-- Location: LCFF_X41_Y26_N13
\takt_counter[31]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~92_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(31));

-- Location: LCFF_X45_Y25_N23
\counter[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~17_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(6));

-- Location: LCFF_X42_Y25_N27
\counter[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~38_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(13));

-- Location: LCFF_X42_Y25_N5
\counter[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~41_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(14));

-- Location: LCFF_X42_Y25_N7
\counter[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~44_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(15));

-- Location: LCCOMB_X43_Y25_N12
\Equal1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (!counter(12) & (!counter(14) & (!counter(15) & !counter(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(12),
	datab => counter(14),
	datac => counter(15),
	datad => counter(13),
	combout => \Equal1~2_combout\);

-- Location: LCFF_X42_Y25_N21
\counter[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~53_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(18));

-- Location: LCCOMB_X45_Y23_N30
\RxD_2_out~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_2_out~0_combout\ = (!\RST~combout\ & \currentState.RECEIVE~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~combout\,
	datad => \currentState.RECEIVE~regout\,
	combout => \RxD_2_out~0_combout\);

-- Location: LCCOMB_X41_Y26_N22
\process_0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~8_combout\ = (takt_counter(31) & (!takt_counter(7) & !takt_counter(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(31),
	datab => takt_counter(7),
	datad => takt_counter(8),
	combout => \process_0~8_combout\);

-- Location: LCCOMB_X44_Y26_N26
\process_0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~12_combout\ = (takt_counter(5) & (takt_counter(2) & (takt_counter(9) & takt_counter(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(5),
	datab => takt_counter(2),
	datac => takt_counter(9),
	datad => takt_counter(3),
	combout => \process_0~12_combout\);

-- Location: LCCOMB_X44_Y26_N4
\process_0~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~13_combout\ = (takt_counter(11) & \process_0~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => takt_counter(11),
	datad => \process_0~12_combout\,
	combout => \process_0~13_combout\);

-- Location: LCFF_X45_Y17_N3
\dataBuffer[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \rm_dec|maj2|LessThan0~0_combout\,
	ena => \RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dataBuffer(2));

-- Location: LCCOMB_X42_Y26_N12
\Add0~67\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~67_combout\ = (\Add0~32_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~32_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~67_combout\);

-- Location: LCCOMB_X42_Y26_N24
\Add0~73\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~73_combout\ = (\Add0~44_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~44_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~73_combout\);

-- Location: LCCOMB_X41_Y26_N12
\Add0~92\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~92_combout\ = (\takt_counter[31]~3_combout\) # (!\Add0~90_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \takt_counter[31]~3_combout\,
	datad => \Add0~90_combout\,
	combout => \Add0~92_combout\);

-- Location: LCCOMB_X45_Y25_N22
\Add1~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~17_combout\ = (\counter[31]~1_combout\ & \Add1~15_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~15_combout\,
	combout => \Add1~17_combout\);

-- Location: LCCOMB_X42_Y25_N26
\Add1~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~38_combout\ = (\counter[31]~1_combout\ & \Add1~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~36_combout\,
	combout => \Add1~38_combout\);

-- Location: LCCOMB_X42_Y25_N4
\Add1~41\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~41_combout\ = (\counter[31]~1_combout\ & \Add1~39_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~39_combout\,
	combout => \Add1~41_combout\);

-- Location: LCCOMB_X42_Y25_N6
\Add1~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~44_combout\ = (\Add1~42_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add1~42_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~44_combout\);

-- Location: LCCOMB_X42_Y25_N20
\Add1~53\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~53_combout\ = (\counter[31]~1_combout\ & \Add1~51_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~51_combout\,
	combout => \Add1~53_combout\);

-- Location: LCCOMB_X44_Y25_N10
\Selector46~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector46~0_combout\ = (\currentState.DECODE~regout\) # ((\currentState.TRANSMIT~regout\ & ((!\process_0~21_combout\) # (!\LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.DECODE~regout\,
	datab => \LessThan0~3_combout\,
	datac => \currentState.TRANSMIT~regout\,
	datad => \process_0~21_combout\,
	combout => \Selector46~0_combout\);

-- Location: LCCOMB_X46_Y17_N16
\rm_dec|maj1|scalar~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj1|scalar~3_combout\ = Parallel1(5) $ (Parallel1(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(5),
	datad => Parallel1(4),
	combout => \rm_dec|maj1|scalar~3_combout\);

-- Location: LCCOMB_X45_Y17_N24
\rm_dec|maj2|Add2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj2|Add2~0_combout\ = (Parallel1(7) & (!Parallel1(5) & (Parallel1(6) $ (Parallel1(4))))) # (!Parallel1(7) & (Parallel1(5) & (Parallel1(6) $ (Parallel1(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(7),
	datab => Parallel1(6),
	datac => Parallel1(5),
	datad => Parallel1(4),
	combout => \rm_dec|maj2|Add2~0_combout\);

-- Location: LCCOMB_X45_Y17_N10
\rm_dec|maj3|scalar~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj3|scalar~0_combout\ = Parallel1(0) $ (Parallel1(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(0),
	datad => Parallel1(4),
	combout => \rm_dec|maj3|scalar~0_combout\);

-- Location: LCCOMB_X46_Y17_N2
\rm_dec|last_maj|ones~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|ones~0_combout\ = (\rm_dec|maj1|scalar~3_combout\ & ((\rm_dec|maj1|scalar~1_combout\) # ((!\rm_dec|maj1|scalar~0_combout\ & !\rm_dec|maj1|scalar~2_combout\)))) # (!\rm_dec|maj1|scalar~3_combout\ & ((\rm_dec|maj1|scalar~1_combout\ & 
-- ((\rm_dec|maj1|scalar~0_combout\) # (\rm_dec|maj1|scalar~2_combout\))) # (!\rm_dec|maj1|scalar~1_combout\ & ((!\rm_dec|maj1|scalar~2_combout\) # (!\rm_dec|maj1|scalar~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110111011011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj1|scalar~3_combout\,
	datab => \rm_dec|maj1|scalar~1_combout\,
	datac => \rm_dec|maj1|scalar~0_combout\,
	datad => \rm_dec|maj1|scalar~2_combout\,
	combout => \rm_dec|last_maj|ones~0_combout\);

-- Location: LCCOMB_X47_Y17_N24
\rm_dec|last_maj|Add9~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add9~1_combout\ = \rm_dec|last_maj|Add7~0_combout\ $ (\rm_dec|last_maj|Add3~0_combout\ $ (((\rm_dec|last_maj|Add2~3_combout\ & !\rm_dec|last_maj|Add6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110111010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add2~3_combout\,
	datab => \rm_dec|last_maj|Add6~0_combout\,
	datac => \rm_dec|last_maj|Add7~0_combout\,
	datad => \rm_dec|last_maj|Add3~0_combout\,
	combout => \rm_dec|last_maj|Add9~1_combout\);

-- Location: LCCOMB_X46_Y17_N8
\rm_dec|last_maj|Add8~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add8~0_combout\ = \rm_dec|last_maj|ones~0_combout\ $ (\rm_dec|last_maj|Add6~0_combout\ $ (\rm_dec|last_maj|vec_cobined~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rm_dec|last_maj|ones~0_combout\,
	datac => \rm_dec|last_maj|Add6~0_combout\,
	datad => \rm_dec|last_maj|vec_cobined~1_combout\,
	combout => \rm_dec|last_maj|Add8~0_combout\);

-- Location: PIN_V2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\RST~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_RST,
	combout => \RST~combout\);

-- Location: PIN_N2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLK~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLK,
	combout => \CLK~combout\);

-- Location: CLKCTRL_G2
\CLK~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLK~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLK~clkctrl_outclk\);

-- Location: PIN_C25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\RxD~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_RxD,
	combout => \RxD~combout\);

-- Location: LCCOMB_X44_Y25_N18
\counter[31]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~0_combout\ = (!\RST~combout\ & !\currentState.DECODE~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \RST~combout\,
	datad => \currentState.DECODE~regout\,
	combout => \counter[31]~0_combout\);

-- Location: LCCOMB_X41_Y25_N4
\Add1~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~5_combout\ = (counter(2) & (\Add1~4\ $ (GND))) # (!counter(2) & (!\Add1~4\ & VCC))
-- \Add1~6\ = CARRY((counter(2) & !\Add1~4\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(2),
	datad => VCC,
	cin => \Add1~4\,
	combout => \Add1~5_combout\,
	cout => \Add1~6\);

-- Location: LCCOMB_X41_Y25_N6
\Add1~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~7_combout\ = (counter(3) & (!\Add1~6\)) # (!counter(3) & ((\Add1~6\) # (GND)))
-- \Add1~8\ = CARRY((!\Add1~6\) # (!counter(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(3),
	datad => VCC,
	cin => \Add1~6\,
	combout => \Add1~7_combout\,
	cout => \Add1~8\);

-- Location: LCCOMB_X45_Y25_N10
\Add1~90\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~90_combout\ = (\Add1~7_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~7_combout\,
	datac => \counter[31]~1_combout\,
	combout => \Add1~90_combout\);

-- Location: LCCOMB_X44_Y25_N12
\takt_counter[31]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \takt_counter[31]~1_combout\ = (\RST~combout\) # (!\currentState.IDLE~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \RST~combout\,
	datad => \currentState.IDLE~regout\,
	combout => \takt_counter[31]~1_combout\);

-- Location: LCCOMB_X41_Y24_N28
\Add1~87\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~87_combout\ = (counter(30) & (\Add1~85\ $ (GND))) # (!counter(30) & (!\Add1~85\ & VCC))
-- \Add1~88\ = CARRY((counter(30) & !\Add1~85\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(30),
	datad => VCC,
	cin => \Add1~85\,
	combout => \Add1~87_combout\,
	cout => \Add1~88\);

-- Location: LCCOMB_X42_Y24_N22
\Add1~89\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~89_combout\ = (\counter[31]~1_combout\ & \Add1~87_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~87_combout\,
	combout => \Add1~89_combout\);

-- Location: LCFF_X42_Y24_N23
\counter[30]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~89_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(30));

-- Location: LCCOMB_X43_Y25_N10
\Equal1~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~6_combout\ = (!counter(25) & (!counter(24) & (!counter(27) & !counter(26))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(25),
	datab => counter(24),
	datac => counter(27),
	datad => counter(26),
	combout => \Equal1~6_combout\);

-- Location: LCCOMB_X43_Y25_N28
\Equal1~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~7_combout\ = (!counter(28) & (!counter(29) & (!counter(30) & \Equal1~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(28),
	datab => counter(29),
	datac => counter(30),
	datad => \Equal1~6_combout\,
	combout => \Equal1~7_combout\);

-- Location: LCCOMB_X43_Y25_N26
\Equal1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = (!counter(11) & (!counter(10) & (!counter(8) & !counter(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(11),
	datab => counter(10),
	datac => counter(8),
	datad => counter(9),
	combout => \Equal1~1_combout\);

-- Location: LCCOMB_X41_Y25_N14
\Add1~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~18_combout\ = (counter(7) & (!\Add1~16\)) # (!counter(7) & ((\Add1~16\) # (GND)))
-- \Add1~19\ = CARRY((!\Add1~16\) # (!counter(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(7),
	datad => VCC,
	cin => \Add1~16\,
	combout => \Add1~18_combout\,
	cout => \Add1~19\);

-- Location: LCCOMB_X45_Y25_N16
\Add1~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~20_combout\ = (\counter[31]~1_combout\ & \Add1~18_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~18_combout\,
	combout => \Add1~20_combout\);

-- Location: LCFF_X45_Y25_N17
\counter[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~20_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(7));

-- Location: LCCOMB_X43_Y25_N24
\Equal1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = (!counter(6) & (!counter(7) & (!counter(4) & !counter(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(6),
	datab => counter(7),
	datac => counter(4),
	datad => counter(5),
	combout => \Equal1~0_combout\);

-- Location: LCCOMB_X41_Y24_N6
\Add1~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~54_combout\ = (counter(19) & (!\Add1~52\)) # (!counter(19) & ((\Add1~52\) # (GND)))
-- \Add1~55\ = CARRY((!\Add1~52\) # (!counter(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(19),
	datad => VCC,
	cin => \Add1~52\,
	combout => \Add1~54_combout\,
	cout => \Add1~55\);

-- Location: LCCOMB_X42_Y25_N30
\Add1~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~56_combout\ = (\Add1~54_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add1~54_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~56_combout\);

-- Location: LCFF_X42_Y25_N31
\counter[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~56_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(19));

-- Location: LCCOMB_X41_Y24_N2
\Add1~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~48_combout\ = (counter(17) & (!\Add1~46\)) # (!counter(17) & ((\Add1~46\) # (GND)))
-- \Add1~49\ = CARRY((!\Add1~46\) # (!counter(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(17),
	datad => VCC,
	cin => \Add1~46\,
	combout => \Add1~48_combout\,
	cout => \Add1~49\);

-- Location: LCCOMB_X42_Y25_N10
\Add1~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~50_combout\ = (\counter[31]~1_combout\ & \Add1~48_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~48_combout\,
	combout => \Add1~50_combout\);

-- Location: LCFF_X42_Y25_N11
\counter[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~50_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(17));

-- Location: LCCOMB_X43_Y25_N30
\Equal1~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~3_combout\ = (!counter(18) & (!counter(16) & (!counter(19) & !counter(17))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(18),
	datab => counter(16),
	datac => counter(19),
	datad => counter(17),
	combout => \Equal1~3_combout\);

-- Location: LCCOMB_X43_Y25_N8
\Equal1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~4_combout\ = (\Equal1~2_combout\ & (\Equal1~1_combout\ & (\Equal1~0_combout\ & \Equal1~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~2_combout\,
	datab => \Equal1~1_combout\,
	datac => \Equal1~0_combout\,
	datad => \Equal1~3_combout\,
	combout => \Equal1~4_combout\);

-- Location: LCCOMB_X43_Y26_N28
\Equal1~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~11_combout\ = (\Equal1~5_combout\ & (\Equal1~9_combout\ & (\Equal1~7_combout\ & \Equal1~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~5_combout\,
	datab => \Equal1~9_combout\,
	datac => \Equal1~7_combout\,
	datad => \Equal1~4_combout\,
	combout => \Equal1~11_combout\);

-- Location: LCCOMB_X44_Y25_N14
\takt_counter[31]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \takt_counter[31]~2_combout\ = (!counter(1) & (counter(0) & (!\process_0~14_combout\ & \Equal1~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => counter(0),
	datac => \process_0~14_combout\,
	datad => \Equal1~11_combout\,
	combout => \takt_counter[31]~2_combout\);

-- Location: LCCOMB_X44_Y25_N16
\Selector44~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector44~0_combout\ = (\currentState.RECEIVE~regout\ & (((!\LessThan0~3_combout\) # (!\takt_counter[31]~2_combout\)))) # (!\currentState.RECEIVE~regout\ & (\currentState.START_BIT~regout\ & ((!\LessThan0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.START_BIT~regout\,
	datab => \takt_counter[31]~2_combout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \LessThan0~3_combout\,
	combout => \Selector44~0_combout\);

-- Location: LCFF_X44_Y25_N17
\currentState.RECEIVE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector44~0_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \currentState.RECEIVE~regout\);

-- Location: LCCOMB_X44_Y25_N4
\Selector43~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector43~0_combout\ = (\currentState.IDLE~regout\ & (((\currentState.START_BIT~regout\ & \LessThan0~3_combout\)))) # (!\currentState.IDLE~regout\ & (((\currentState.START_BIT~regout\ & \LessThan0~3_combout\)) # (!\RxD~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.IDLE~regout\,
	datab => \RxD~combout\,
	datac => \currentState.START_BIT~regout\,
	datad => \LessThan0~3_combout\,
	combout => \Selector43~0_combout\);

-- Location: LCFF_X44_Y25_N5
\currentState.START_BIT\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector43~0_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \currentState.START_BIT~regout\);

-- Location: LCCOMB_X44_Y25_N26
\takt_counter[31]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \takt_counter[31]~0_combout\ = (!\LessThan0~3_combout\ & ((\currentState.TRANSMIT~regout\) # ((\currentState.RECEIVE~regout\) # (\currentState.START_BIT~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datab => \LessThan0~3_combout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \currentState.START_BIT~regout\,
	combout => \takt_counter[31]~0_combout\);

-- Location: LCCOMB_X44_Y25_N0
\takt_counter[31]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \takt_counter[31]~3_combout\ = (\takt_counter[31]~1_combout\) # ((\takt_counter[31]~0_combout\) # ((\currentState.RECEIVE~regout\ & \takt_counter[31]~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.RECEIVE~regout\,
	datab => \takt_counter[31]~1_combout\,
	datac => \takt_counter[31]~2_combout\,
	datad => \takt_counter[31]~0_combout\,
	combout => \takt_counter[31]~3_combout\);

-- Location: LCCOMB_X41_Y26_N8
\Add0~82\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~82_combout\ = (\Add0~10_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~10_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~82_combout\);

-- Location: LCFF_X41_Y26_N9
\takt_counter[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~82_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(5));

-- Location: LCCOMB_X44_Y26_N8
\Add0~95\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~95_combout\ = (\Add0~4_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~4_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~95_combout\);

-- Location: LCFF_X44_Y26_N9
\takt_counter[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~95_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(2));

-- Location: LCCOMB_X40_Y26_N0
\Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = takt_counter(0) $ (GND)
-- \Add0~1\ = CARRY(!takt_counter(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(0),
	datad => VCC,
	combout => \Add0~0_combout\,
	cout => \Add0~1\);

-- Location: LCCOMB_X44_Y26_N0
\Add0~93\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~93_combout\ = (\takt_counter[31]~3_combout\) # (!\Add0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~0_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~93_combout\);

-- Location: LCFF_X44_Y26_N1
\takt_counter[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~93_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(0));

-- Location: LCCOMB_X40_Y26_N6
\Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~6_combout\ = (takt_counter(3) & (!\Add0~5\)) # (!takt_counter(3) & ((\Add0~5\) # (GND)))
-- \Add0~7\ = CARRY((!\Add0~5\) # (!takt_counter(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(3),
	datad => VCC,
	cin => \Add0~5\,
	combout => \Add0~6_combout\,
	cout => \Add0~7\);

-- Location: LCCOMB_X41_Y26_N14
\Add0~80\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~80_combout\ = (\Add0~6_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~6_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~80_combout\);

-- Location: LCFF_X41_Y26_N15
\takt_counter[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~80_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(3));

-- Location: LCCOMB_X40_Y26_N8
\Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~8_combout\ = (takt_counter(4) & (\Add0~7\ $ (GND))) # (!takt_counter(4) & (!\Add0~7\ & VCC))
-- \Add0~9\ = CARRY((takt_counter(4) & !\Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(4),
	datad => VCC,
	cin => \Add0~7\,
	combout => \Add0~8_combout\,
	cout => \Add0~9\);

-- Location: LCCOMB_X44_Y26_N24
\Add0~81\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~81_combout\ = (\Add0~8_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~8_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~81_combout\);

-- Location: LCFF_X44_Y26_N25
\takt_counter[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~81_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(4));

-- Location: LCCOMB_X40_Y26_N16
\Add0~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~16_combout\ = (takt_counter(8) & (\Add0~15\ $ (GND))) # (!takt_counter(8) & (!\Add0~15\ & VCC))
-- \Add0~17\ = CARRY((takt_counter(8) & !\Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(8),
	datad => VCC,
	cin => \Add0~15\,
	combout => \Add0~16_combout\,
	cout => \Add0~17\);

-- Location: LCCOMB_X41_Y26_N30
\Add0~86\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~86_combout\ = (\Add0~16_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~16_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~86_combout\);

-- Location: LCFF_X41_Y26_N31
\takt_counter[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~86_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(8));

-- Location: LCCOMB_X40_Y26_N18
\Add0~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~18_combout\ = (takt_counter(9) & (!\Add0~17\)) # (!takt_counter(9) & ((\Add0~17\) # (GND)))
-- \Add0~19\ = CARRY((!\Add0~17\) # (!takt_counter(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(9),
	datad => VCC,
	cin => \Add0~17\,
	combout => \Add0~18_combout\,
	cout => \Add0~19\);

-- Location: LCCOMB_X40_Y26_N20
\Add0~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~20_combout\ = (takt_counter(10) & (\Add0~19\ $ (GND))) # (!takt_counter(10) & (!\Add0~19\ & VCC))
-- \Add0~21\ = CARRY((takt_counter(10) & !\Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(10),
	datad => VCC,
	cin => \Add0~19\,
	combout => \Add0~20_combout\,
	cout => \Add0~21\);

-- Location: LCCOMB_X40_Y26_N22
\Add0~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~22_combout\ = (takt_counter(11) & (!\Add0~21\)) # (!takt_counter(11) & ((\Add0~21\) # (GND)))
-- \Add0~23\ = CARRY((!\Add0~21\) # (!takt_counter(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(11),
	datad => VCC,
	cin => \Add0~21\,
	combout => \Add0~22_combout\,
	cout => \Add0~23\);

-- Location: LCCOMB_X41_Y26_N18
\Add0~88\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~88_combout\ = (\Add0~22_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add0~22_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~88_combout\);

-- Location: LCFF_X41_Y26_N19
\takt_counter[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~88_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(11));

-- Location: LCCOMB_X40_Y26_N24
\Add0~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~24_combout\ = (takt_counter(12) & (\Add0~23\ $ (GND))) # (!takt_counter(12) & (!\Add0~23\ & VCC))
-- \Add0~25\ = CARRY((takt_counter(12) & !\Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(12),
	datad => VCC,
	cin => \Add0~23\,
	combout => \Add0~24_combout\,
	cout => \Add0~25\);

-- Location: LCCOMB_X45_Y23_N12
\Add0~89\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~89_combout\ = (\Add0~24_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~24_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~89_combout\);

-- Location: LCFF_X45_Y23_N13
\takt_counter[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~89_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(12));

-- Location: LCCOMB_X41_Y26_N4
\Add0~85\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~85_combout\ = (\Add0~14_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~14_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~85_combout\);

-- Location: LCFF_X41_Y26_N5
\takt_counter[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~85_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(7));

-- Location: LCCOMB_X41_Y26_N2
\Add0~84\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~84_combout\ = (\Add0~18_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add0~18_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~84_combout\);

-- Location: LCFF_X41_Y26_N3
\takt_counter[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~84_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(9));

-- Location: LCCOMB_X41_Y26_N16
\LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = (!takt_counter(7) & (!takt_counter(8) & !takt_counter(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(7),
	datac => takt_counter(8),
	datad => takt_counter(9),
	combout => \LessThan0~1_combout\);

-- Location: LCCOMB_X44_Y26_N10
\Add0~83\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~83_combout\ = (\Add0~12_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~12_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~83_combout\);

-- Location: LCFF_X44_Y26_N11
\takt_counter[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~83_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(6));

-- Location: LCCOMB_X44_Y26_N12
\LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ((!takt_counter(5) & ((!takt_counter(3)) # (!takt_counter(4))))) # (!takt_counter(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(4),
	datab => takt_counter(6),
	datac => takt_counter(3),
	datad => takt_counter(5),
	combout => \LessThan0~0_combout\);

-- Location: LCCOMB_X44_Y25_N24
\LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = (!takt_counter(11) & (((\LessThan0~1_combout\ & \LessThan0~0_combout\)) # (!takt_counter(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(10),
	datab => takt_counter(11),
	datac => \LessThan0~1_combout\,
	datad => \LessThan0~0_combout\,
	combout => \LessThan0~2_combout\);

-- Location: LCCOMB_X40_Y26_N26
\Add0~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~26_combout\ = (takt_counter(13) & (!\Add0~25\)) # (!takt_counter(13) & ((\Add0~25\) # (GND)))
-- \Add0~27\ = CARRY((!\Add0~25\) # (!takt_counter(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(13),
	datad => VCC,
	cin => \Add0~25\,
	combout => \Add0~26_combout\,
	cout => \Add0~27\);

-- Location: LCCOMB_X42_Y26_N14
\Add0~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~64_combout\ = (\Add0~26_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~26_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~64_combout\);

-- Location: LCFF_X42_Y26_N15
\takt_counter[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~64_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(13));

-- Location: LCCOMB_X40_Y26_N28
\Add0~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~28_combout\ = (takt_counter(14) & (\Add0~27\ $ (GND))) # (!takt_counter(14) & (!\Add0~27\ & VCC))
-- \Add0~29\ = CARRY((takt_counter(14) & !\Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(14),
	datad => VCC,
	cin => \Add0~27\,
	combout => \Add0~28_combout\,
	cout => \Add0~29\);

-- Location: LCCOMB_X42_Y26_N16
\Add0~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~65_combout\ = (\Add0~28_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~28_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~65_combout\);

-- Location: LCFF_X42_Y26_N17
\takt_counter[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~65_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(14));

-- Location: LCCOMB_X40_Y26_N30
\Add0~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~30_combout\ = (takt_counter(15) & (!\Add0~29\)) # (!takt_counter(15) & ((\Add0~29\) # (GND)))
-- \Add0~31\ = CARRY((!\Add0~29\) # (!takt_counter(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(15),
	datad => VCC,
	cin => \Add0~29\,
	combout => \Add0~30_combout\,
	cout => \Add0~31\);

-- Location: LCCOMB_X42_Y26_N18
\Add0~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~66_combout\ = (\Add0~30_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~30_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~66_combout\);

-- Location: LCFF_X42_Y26_N19
\takt_counter[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~66_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(15));

-- Location: LCCOMB_X40_Y25_N2
\Add0~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~34_combout\ = (takt_counter(17) & (!\Add0~33\)) # (!takt_counter(17) & ((\Add0~33\) # (GND)))
-- \Add0~35\ = CARRY((!\Add0~33\) # (!takt_counter(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(17),
	datad => VCC,
	cin => \Add0~33\,
	combout => \Add0~34_combout\,
	cout => \Add0~35\);

-- Location: LCCOMB_X42_Y26_N8
\Add0~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~68_combout\ = (\Add0~34_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~34_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~68_combout\);

-- Location: LCFF_X42_Y26_N9
\takt_counter[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~68_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(17));

-- Location: LCCOMB_X40_Y25_N4
\Add0~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~36_combout\ = (takt_counter(18) & (\Add0~35\ $ (GND))) # (!takt_counter(18) & (!\Add0~35\ & VCC))
-- \Add0~37\ = CARRY((takt_counter(18) & !\Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(18),
	datad => VCC,
	cin => \Add0~35\,
	combout => \Add0~36_combout\,
	cout => \Add0~37\);

-- Location: LCCOMB_X41_Y26_N24
\Add0~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~69_combout\ = (!\takt_counter[31]~3_combout\ & \Add0~36_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \takt_counter[31]~3_combout\,
	datad => \Add0~36_combout\,
	combout => \Add0~69_combout\);

-- Location: LCFF_X41_Y26_N25
\takt_counter[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~69_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(18));

-- Location: LCCOMB_X40_Y25_N6
\Add0~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~38_combout\ = (takt_counter(19) & (!\Add0~37\)) # (!takt_counter(19) & ((\Add0~37\) # (GND)))
-- \Add0~39\ = CARRY((!\Add0~37\) # (!takt_counter(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(19),
	datad => VCC,
	cin => \Add0~37\,
	combout => \Add0~38_combout\,
	cout => \Add0~39\);

-- Location: LCCOMB_X41_Y26_N10
\Add0~70\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~70_combout\ = (\Add0~38_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~38_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~70_combout\);

-- Location: LCFF_X41_Y26_N11
\takt_counter[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~70_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(19));

-- Location: LCCOMB_X40_Y25_N8
\Add0~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~40_combout\ = (takt_counter(20) & (\Add0~39\ $ (GND))) # (!takt_counter(20) & (!\Add0~39\ & VCC))
-- \Add0~41\ = CARRY((takt_counter(20) & !\Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(20),
	datad => VCC,
	cin => \Add0~39\,
	combout => \Add0~40_combout\,
	cout => \Add0~41\);

-- Location: LCCOMB_X42_Y26_N10
\Add0~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~71_combout\ = (\Add0~40_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~40_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~71_combout\);

-- Location: LCFF_X42_Y26_N11
\takt_counter[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~71_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(20));

-- Location: LCCOMB_X40_Y25_N14
\Add0~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~46_combout\ = (takt_counter(23) & (!\Add0~45\)) # (!takt_counter(23) & ((\Add0~45\) # (GND)))
-- \Add0~47\ = CARRY((!\Add0~45\) # (!takt_counter(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(23),
	datad => VCC,
	cin => \Add0~45\,
	combout => \Add0~46_combout\,
	cout => \Add0~47\);

-- Location: LCCOMB_X40_Y25_N16
\Add0~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~48_combout\ = (takt_counter(24) & (\Add0~47\ $ (GND))) # (!takt_counter(24) & (!\Add0~47\ & VCC))
-- \Add0~49\ = CARRY((takt_counter(24) & !\Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(24),
	datad => VCC,
	cin => \Add0~47\,
	combout => \Add0~48_combout\,
	cout => \Add0~49\);

-- Location: LCCOMB_X42_Y26_N4
\Add0~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~75_combout\ = (\Add0~48_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~48_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~75_combout\);

-- Location: LCFF_X42_Y26_N5
\takt_counter[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~75_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(24));

-- Location: LCCOMB_X40_Y25_N18
\Add0~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~50_combout\ = (takt_counter(25) & (!\Add0~49\)) # (!takt_counter(25) & ((\Add0~49\) # (GND)))
-- \Add0~51\ = CARRY((!\Add0~49\) # (!takt_counter(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(25),
	datad => VCC,
	cin => \Add0~49\,
	combout => \Add0~50_combout\,
	cout => \Add0~51\);

-- Location: LCCOMB_X40_Y25_N20
\Add0~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~52_combout\ = (takt_counter(26) & (\Add0~51\ $ (GND))) # (!takt_counter(26) & (!\Add0~51\ & VCC))
-- \Add0~53\ = CARRY((takt_counter(26) & !\Add0~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(26),
	datad => VCC,
	cin => \Add0~51\,
	combout => \Add0~52_combout\,
	cout => \Add0~53\);

-- Location: LCCOMB_X41_Y26_N6
\Add0~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~77_combout\ = (!\takt_counter[31]~3_combout\ & \Add0~52_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \takt_counter[31]~3_combout\,
	datad => \Add0~52_combout\,
	combout => \Add0~77_combout\);

-- Location: LCFF_X41_Y26_N7
\takt_counter[26]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~77_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(26));

-- Location: LCCOMB_X40_Y25_N22
\Add0~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~54_combout\ = (takt_counter(27) & (!\Add0~53\)) # (!takt_counter(27) & ((\Add0~53\) # (GND)))
-- \Add0~55\ = CARRY((!\Add0~53\) # (!takt_counter(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(27),
	datad => VCC,
	cin => \Add0~53\,
	combout => \Add0~54_combout\,
	cout => \Add0~55\);

-- Location: LCCOMB_X41_Y26_N0
\Add0~78\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~78_combout\ = (!\takt_counter[31]~3_combout\ & \Add0~54_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \takt_counter[31]~3_combout\,
	datad => \Add0~54_combout\,
	combout => \Add0~78_combout\);

-- Location: LCFF_X41_Y26_N1
\takt_counter[27]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~78_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(27));

-- Location: LCCOMB_X40_Y25_N24
\Add0~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~56_combout\ = (takt_counter(28) & (\Add0~55\ $ (GND))) # (!takt_counter(28) & (!\Add0~55\ & VCC))
-- \Add0~57\ = CARRY((takt_counter(28) & !\Add0~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(28),
	datad => VCC,
	cin => \Add0~55\,
	combout => \Add0~56_combout\,
	cout => \Add0~57\);

-- Location: LCCOMB_X41_Y26_N26
\Add0~79\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~79_combout\ = (\Add0~56_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~56_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~79_combout\);

-- Location: LCFF_X41_Y26_N27
\takt_counter[28]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~79_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(28));

-- Location: LCCOMB_X40_Y25_N26
\Add0~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~58_combout\ = (takt_counter(29) & (!\Add0~57\)) # (!takt_counter(29) & ((\Add0~57\) # (GND)))
-- \Add0~59\ = CARRY((!\Add0~57\) # (!takt_counter(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(29),
	datad => VCC,
	cin => \Add0~57\,
	combout => \Add0~58_combout\,
	cout => \Add0~59\);

-- Location: LCCOMB_X42_Y26_N26
\Add0~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~63_combout\ = (\Add0~61_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~61_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~63_combout\);

-- Location: LCFF_X42_Y26_N27
\takt_counter[30]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~63_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(30));

-- Location: LCCOMB_X42_Y26_N0
\Add0~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~60_combout\ = (\Add0~58_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~58_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~60_combout\);

-- Location: LCFF_X42_Y26_N1
\takt_counter[29]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~60_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(29));

-- Location: LCCOMB_X42_Y26_N22
\Add0~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~72_combout\ = (\Add0~42_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~42_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~72_combout\);

-- Location: LCFF_X42_Y26_N23
\takt_counter[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~72_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(21));

-- Location: LCCOMB_X42_Y26_N2
\Add0~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~74_combout\ = (\Add0~46_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~46_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~74_combout\);

-- Location: LCFF_X42_Y26_N3
\takt_counter[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~74_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(23));

-- Location: LCCOMB_X42_Y26_N6
\process_0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~5_combout\ = (!takt_counter(22) & (!takt_counter(21) & (!takt_counter(24) & !takt_counter(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(22),
	datab => takt_counter(21),
	datac => takt_counter(24),
	datad => takt_counter(23),
	combout => \process_0~5_combout\);

-- Location: LCCOMB_X42_Y26_N20
\process_0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~4_combout\ = (!takt_counter(20) & (!takt_counter(18) & (!takt_counter(17) & !takt_counter(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(20),
	datab => takt_counter(18),
	datac => takt_counter(17),
	datad => takt_counter(19),
	combout => \process_0~4_combout\);

-- Location: LCCOMB_X41_Y26_N20
\Add0~76\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~76_combout\ = (!\takt_counter[31]~3_combout\ & \Add0~50_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \takt_counter[31]~3_combout\,
	datad => \Add0~50_combout\,
	combout => \Add0~76_combout\);

-- Location: LCFF_X41_Y26_N21
\takt_counter[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~76_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(25));

-- Location: LCCOMB_X41_Y26_N28
\process_0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~6_combout\ = (!takt_counter(26) & (!takt_counter(28) & (!takt_counter(25) & !takt_counter(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(26),
	datab => takt_counter(28),
	datac => takt_counter(25),
	datad => takt_counter(27),
	combout => \process_0~6_combout\);

-- Location: LCCOMB_X43_Y26_N18
\process_0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~7_combout\ = (\process_0~3_combout\ & (\process_0~5_combout\ & (\process_0~4_combout\ & \process_0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~3_combout\,
	datab => \process_0~5_combout\,
	datac => \process_0~4_combout\,
	datad => \process_0~6_combout\,
	combout => \process_0~7_combout\);

-- Location: LCCOMB_X43_Y26_N14
\process_0~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~22_combout\ = (!takt_counter(30) & (!takt_counter(29) & \process_0~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(30),
	datac => takt_counter(29),
	datad => \process_0~7_combout\,
	combout => \process_0~22_combout\);

-- Location: LCCOMB_X44_Y25_N2
\LessThan0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = ((\process_0~22_combout\ & ((\LessThan0~2_combout\) # (!takt_counter(12))))) # (!takt_counter(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(31),
	datab => takt_counter(12),
	datac => \LessThan0~2_combout\,
	datad => \process_0~22_combout\,
	combout => \LessThan0~3_combout\);

-- Location: LCCOMB_X44_Y25_N30
\currentState~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \currentState~12_combout\ = (\RST~combout\) # ((\RxD~combout\ & !\currentState.IDLE~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \RST~combout\,
	datac => \RxD~combout\,
	datad => \currentState.IDLE~regout\,
	combout => \currentState~12_combout\);

-- Location: LCCOMB_X44_Y26_N18
\Add0~94\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~94_combout\ = (\Add0~2_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add0~2_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~94_combout\);

-- Location: LCFF_X44_Y26_N19
\takt_counter[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~94_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(1));

-- Location: LCCOMB_X44_Y26_N2
\process_0~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~18_combout\ = (takt_counter(4) & (!takt_counter(0) & (takt_counter(2) & takt_counter(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(4),
	datab => takt_counter(0),
	datac => takt_counter(2),
	datad => takt_counter(1),
	combout => \process_0~18_combout\);

-- Location: LCCOMB_X44_Y26_N6
\Add0~87\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add0~87_combout\ = (\Add0~20_combout\ & !\takt_counter[31]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add0~20_combout\,
	datad => \takt_counter[31]~3_combout\,
	combout => \Add0~87_combout\);

-- Location: LCFF_X44_Y26_N7
\takt_counter[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add0~87_combout\,
	ena => \ALT_INV_RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => takt_counter(10));

-- Location: LCCOMB_X44_Y26_N20
\process_0~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~19_combout\ = (takt_counter(6) & (counter(0) & (takt_counter(12) & takt_counter(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(6),
	datab => counter(0),
	datac => takt_counter(12),
	datad => takt_counter(10),
	combout => \process_0~19_combout\);

-- Location: LCCOMB_X44_Y26_N22
\process_0~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~15_combout\ = (!takt_counter(5) & (!takt_counter(3) & (!takt_counter(9) & !takt_counter(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(5),
	datab => takt_counter(3),
	datac => takt_counter(9),
	datad => takt_counter(11),
	combout => \process_0~15_combout\);

-- Location: LCCOMB_X44_Y26_N30
\process_0~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~20_combout\ = (counter(1) & (\process_0~18_combout\ & (\process_0~19_combout\ & \process_0~15_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => \process_0~18_combout\,
	datac => \process_0~19_combout\,
	datad => \process_0~15_combout\,
	combout => \process_0~20_combout\);

-- Location: LCCOMB_X43_Y26_N6
\process_0~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~21_combout\ = (\process_0~8_combout\ & (\process_0~20_combout\ & (\process_0~22_combout\ & \Equal1~11_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~8_combout\,
	datab => \process_0~20_combout\,
	datac => \process_0~22_combout\,
	datad => \Equal1~11_combout\,
	combout => \process_0~21_combout\);

-- Location: LCCOMB_X44_Y25_N28
\currentState~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \currentState~13_combout\ = (!\currentState~12_combout\ & (((!\process_0~21_combout\) # (!\LessThan0~3_combout\)) # (!\currentState.TRANSMIT~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datab => \LessThan0~3_combout\,
	datac => \currentState~12_combout\,
	datad => \process_0~21_combout\,
	combout => \currentState~13_combout\);

-- Location: LCFF_X44_Y25_N29
\currentState.IDLE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \currentState~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \currentState.IDLE~regout\);

-- Location: LCCOMB_X45_Y25_N28
\LessThan2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan2~0_combout\ = ((!counter(1) & !counter(2))) # (!counter(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => counter(1),
	datac => counter(2),
	datad => counter(3),
	combout => \LessThan2~0_combout\);

-- Location: LCCOMB_X41_Y24_N10
\Add1~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~60_combout\ = (counter(21) & (!\Add1~58\)) # (!counter(21) & ((\Add1~58\) # (GND)))
-- \Add1~61\ = CARRY((!\Add1~58\) # (!counter(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => counter(21),
	datad => VCC,
	cin => \Add1~58\,
	combout => \Add1~60_combout\,
	cout => \Add1~61\);

-- Location: LCCOMB_X42_Y25_N2
\Add1~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~62_combout\ = (\counter[31]~1_combout\ & \Add1~60_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~60_combout\,
	combout => \Add1~62_combout\);

-- Location: LCFF_X42_Y25_N3
\counter[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~62_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(21));

-- Location: LCCOMB_X42_Y24_N8
\Add1~68\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~68_combout\ = (\Add1~66_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Add1~66_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~68_combout\);

-- Location: LCFF_X42_Y24_N9
\counter[23]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~68_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(23));

-- Location: LCCOMB_X42_Y25_N22
\Equal1~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~5_combout\ = (!counter(22) & (!counter(21) & (!counter(20) & !counter(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(22),
	datab => counter(21),
	datac => counter(20),
	datad => counter(23),
	combout => \Equal1~5_combout\);

-- Location: LCCOMB_X43_Y25_N22
\Equal1~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~8_combout\ = (\Equal1~5_combout\ & (\Equal1~4_combout\ & \Equal1~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal1~5_combout\,
	datac => \Equal1~4_combout\,
	datad => \Equal1~7_combout\,
	combout => \Equal1~8_combout\);

-- Location: LCCOMB_X43_Y25_N20
\counter[31]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~2_combout\ = (counter(31) & ((!\Equal1~8_combout\) # (!\LessThan2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \LessThan2~0_combout\,
	datac => counter(31),
	datad => \Equal1~8_combout\,
	combout => \counter[31]~2_combout\);

-- Location: LCCOMB_X43_Y26_N24
\counter[31]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~3_combout\ = (\process_0~21_combout\) # ((\process_0~16_combout\ & !\counter[31]~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~16_combout\,
	datac => \counter[31]~2_combout\,
	datad => \process_0~21_combout\,
	combout => \counter[31]~3_combout\);

-- Location: LCCOMB_X43_Y26_N10
\counter[31]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~4_combout\ = (\currentState.RECEIVE~regout\ & (((!\Equal1~10_combout\ & !\process_0~14_combout\)) # (!\LessThan0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.RECEIVE~regout\,
	datab => \Equal1~10_combout\,
	datac => \process_0~14_combout\,
	datad => \LessThan0~3_combout\,
	combout => \counter[31]~4_combout\);

-- Location: LCCOMB_X43_Y26_N12
\counter[31]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~5_combout\ = (\currentState.TRANSMIT~regout\ & (((!\counter[31]~3_combout\)) # (!\LessThan0~3_combout\))) # (!\currentState.TRANSMIT~regout\ & (((\counter[31]~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111100101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datab => \LessThan0~3_combout\,
	datac => \counter[31]~3_combout\,
	datad => \counter[31]~4_combout\,
	combout => \counter[31]~5_combout\);

-- Location: LCCOMB_X43_Y26_N22
\counter[31]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~6_combout\ = (\RST~combout\) # ((\currentState.IDLE~regout\ & (!\currentState.START_BIT~regout\ & !\counter[31]~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~combout\,
	datab => \currentState.IDLE~regout\,
	datac => \currentState.START_BIT~regout\,
	datad => \counter[31]~5_combout\,
	combout => \counter[31]~6_combout\);

-- Location: LCFF_X45_Y25_N11
\counter[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~90_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(3));

-- Location: LCCOMB_X41_Y25_N8
\Add1~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~9_combout\ = (counter(4) & (\Add1~8\ $ (GND))) # (!counter(4) & (!\Add1~8\ & VCC))
-- \Add1~10\ = CARRY((counter(4) & !\Add1~8\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(4),
	datad => VCC,
	cin => \Add1~8\,
	combout => \Add1~9_combout\,
	cout => \Add1~10\);

-- Location: LCCOMB_X45_Y25_N26
\Add1~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~11_combout\ = (\counter[31]~1_combout\ & \Add1~9_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~9_combout\,
	combout => \Add1~11_combout\);

-- Location: LCFF_X45_Y25_N27
\counter[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~11_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(4));

-- Location: LCCOMB_X41_Y25_N10
\Add1~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~12_combout\ = (counter(5) & (!\Add1~10\)) # (!counter(5) & ((\Add1~10\) # (GND)))
-- \Add1~13\ = CARRY((!\Add1~10\) # (!counter(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(5),
	datad => VCC,
	cin => \Add1~10\,
	combout => \Add1~12_combout\,
	cout => \Add1~13\);

-- Location: LCCOMB_X45_Y25_N4
\Add1~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~14_combout\ = (\counter[31]~1_combout\ & \Add1~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~12_combout\,
	combout => \Add1~14_combout\);

-- Location: LCFF_X45_Y25_N5
\counter[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~14_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(5));

-- Location: LCCOMB_X41_Y25_N16
\Add1~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~21_combout\ = (counter(8) & (\Add1~19\ $ (GND))) # (!counter(8) & (!\Add1~19\ & VCC))
-- \Add1~22\ = CARRY((counter(8) & !\Add1~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(8),
	datad => VCC,
	cin => \Add1~19\,
	combout => \Add1~21_combout\,
	cout => \Add1~22\);

-- Location: LCCOMB_X42_Y25_N0
\Add1~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~23_combout\ = (\Add1~21_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~21_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~23_combout\);

-- Location: LCFF_X42_Y25_N1
\counter[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~23_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(8));

-- Location: LCCOMB_X41_Y25_N18
\Add1~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~24_combout\ = (counter(9) & (!\Add1~22\)) # (!counter(9) & ((\Add1~22\) # (GND)))
-- \Add1~25\ = CARRY((!\Add1~22\) # (!counter(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(9),
	datad => VCC,
	cin => \Add1~22\,
	combout => \Add1~24_combout\,
	cout => \Add1~25\);

-- Location: LCCOMB_X42_Y25_N18
\Add1~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~26_combout\ = (\Add1~24_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~24_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~26_combout\);

-- Location: LCFF_X42_Y25_N19
\counter[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~26_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(9));

-- Location: LCCOMB_X41_Y25_N20
\Add1~27\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~27_combout\ = (counter(10) & (\Add1~25\ $ (GND))) # (!counter(10) & (!\Add1~25\ & VCC))
-- \Add1~28\ = CARRY((counter(10) & !\Add1~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(10),
	datad => VCC,
	cin => \Add1~25\,
	combout => \Add1~27_combout\,
	cout => \Add1~28\);

-- Location: LCCOMB_X42_Y25_N28
\Add1~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~29_combout\ = (\Add1~27_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add1~27_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~29_combout\);

-- Location: LCFF_X42_Y25_N29
\counter[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~29_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(10));

-- Location: LCCOMB_X41_Y25_N22
\Add1~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~30_combout\ = (counter(11) & (!\Add1~28\)) # (!counter(11) & ((\Add1~28\) # (GND)))
-- \Add1~31\ = CARRY((!\Add1~28\) # (!counter(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(11),
	datad => VCC,
	cin => \Add1~28\,
	combout => \Add1~30_combout\,
	cout => \Add1~31\);

-- Location: LCCOMB_X42_Y25_N14
\Add1~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~32_combout\ = (\counter[31]~1_combout\ & \Add1~30_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~30_combout\,
	combout => \Add1~32_combout\);

-- Location: LCFF_X42_Y25_N15
\counter[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~32_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(11));

-- Location: LCCOMB_X41_Y25_N24
\Add1~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~33_combout\ = (counter(12) & (\Add1~31\ $ (GND))) # (!counter(12) & (!\Add1~31\ & VCC))
-- \Add1~34\ = CARRY((counter(12) & !\Add1~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(12),
	datad => VCC,
	cin => \Add1~31\,
	combout => \Add1~33_combout\,
	cout => \Add1~34\);

-- Location: LCCOMB_X42_Y25_N8
\Add1~35\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~35_combout\ = (\counter[31]~1_combout\ & \Add1~33_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~33_combout\,
	combout => \Add1~35_combout\);

-- Location: LCFF_X42_Y25_N9
\counter[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~35_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(12));

-- Location: LCCOMB_X41_Y24_N0
\Add1~45\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~45_combout\ = (counter(16) & (\Add1~43\ $ (GND))) # (!counter(16) & (!\Add1~43\ & VCC))
-- \Add1~46\ = CARRY((counter(16) & !\Add1~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(16),
	datad => VCC,
	cin => \Add1~43\,
	combout => \Add1~45_combout\,
	cout => \Add1~46\);

-- Location: LCCOMB_X42_Y25_N24
\Add1~47\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~47_combout\ = (\Add1~45_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add1~45_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~47_combout\);

-- Location: LCFF_X42_Y25_N25
\counter[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~47_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(16));

-- Location: LCCOMB_X41_Y24_N8
\Add1~57\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~57_combout\ = (counter(20) & (\Add1~55\ $ (GND))) # (!counter(20) & (!\Add1~55\ & VCC))
-- \Add1~58\ = CARRY((counter(20) & !\Add1~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(20),
	datad => VCC,
	cin => \Add1~55\,
	combout => \Add1~57_combout\,
	cout => \Add1~58\);

-- Location: LCCOMB_X42_Y25_N16
\Add1~59\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~59_combout\ = (\counter[31]~1_combout\ & \Add1~57_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~57_combout\,
	combout => \Add1~59_combout\);

-- Location: LCFF_X42_Y25_N17
\counter[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~59_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(20));

-- Location: LCCOMB_X41_Y24_N12
\Add1~63\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~63_combout\ = (counter(22) & (\Add1~61\ $ (GND))) # (!counter(22) & (!\Add1~61\ & VCC))
-- \Add1~64\ = CARRY((counter(22) & !\Add1~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(22),
	datad => VCC,
	cin => \Add1~61\,
	combout => \Add1~63_combout\,
	cout => \Add1~64\);

-- Location: LCCOMB_X42_Y25_N12
\Add1~65\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~65_combout\ = (\counter[31]~1_combout\ & \Add1~63_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~63_combout\,
	combout => \Add1~65_combout\);

-- Location: LCFF_X42_Y25_N13
\counter[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~65_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(22));

-- Location: LCCOMB_X41_Y24_N16
\Add1~69\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~69_combout\ = (counter(24) & (\Add1~67\ $ (GND))) # (!counter(24) & (!\Add1~67\ & VCC))
-- \Add1~70\ = CARRY((counter(24) & !\Add1~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(24),
	datad => VCC,
	cin => \Add1~67\,
	combout => \Add1~69_combout\,
	cout => \Add1~70\);

-- Location: LCCOMB_X42_Y24_N18
\Add1~71\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~71_combout\ = (\Add1~69_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~69_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~71_combout\);

-- Location: LCFF_X42_Y24_N19
\counter[24]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~71_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(24));

-- Location: LCCOMB_X41_Y24_N18
\Add1~72\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~72_combout\ = (counter(25) & (!\Add1~70\)) # (!counter(25) & ((\Add1~70\) # (GND)))
-- \Add1~73\ = CARRY((!\Add1~70\) # (!counter(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(25),
	datad => VCC,
	cin => \Add1~70\,
	combout => \Add1~72_combout\,
	cout => \Add1~73\);

-- Location: LCCOMB_X42_Y24_N12
\Add1~74\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~74_combout\ = (\Add1~72_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~72_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~74_combout\);

-- Location: LCFF_X42_Y24_N13
\counter[25]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~74_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(25));

-- Location: LCCOMB_X41_Y24_N20
\Add1~75\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~75_combout\ = (counter(26) & (\Add1~73\ $ (GND))) # (!counter(26) & (!\Add1~73\ & VCC))
-- \Add1~76\ = CARRY((counter(26) & !\Add1~73\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(26),
	datad => VCC,
	cin => \Add1~73\,
	combout => \Add1~75_combout\,
	cout => \Add1~76\);

-- Location: LCCOMB_X42_Y24_N14
\Add1~77\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~77_combout\ = (\Add1~75_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Add1~75_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~77_combout\);

-- Location: LCFF_X42_Y24_N15
\counter[26]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~77_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(26));

-- Location: LCCOMB_X41_Y24_N22
\Add1~78\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~78_combout\ = (counter(27) & (!\Add1~76\)) # (!counter(27) & ((\Add1~76\) # (GND)))
-- \Add1~79\ = CARRY((!\Add1~76\) # (!counter(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(27),
	datad => VCC,
	cin => \Add1~76\,
	combout => \Add1~78_combout\,
	cout => \Add1~79\);

-- Location: LCCOMB_X42_Y24_N0
\Add1~80\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~80_combout\ = (\Add1~78_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~78_combout\,
	datad => \counter[31]~1_combout\,
	combout => \Add1~80_combout\);

-- Location: LCFF_X42_Y24_N1
\counter[27]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~80_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(27));

-- Location: LCCOMB_X41_Y24_N24
\Add1~81\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~81_combout\ = (counter(28) & (\Add1~79\ $ (GND))) # (!counter(28) & (!\Add1~79\ & VCC))
-- \Add1~82\ = CARRY((counter(28) & !\Add1~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(28),
	datad => VCC,
	cin => \Add1~79\,
	combout => \Add1~81_combout\,
	cout => \Add1~82\);

-- Location: LCCOMB_X42_Y24_N2
\Add1~83\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~83_combout\ = (\counter[31]~1_combout\ & \Add1~81_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~81_combout\,
	combout => \Add1~83_combout\);

-- Location: LCFF_X42_Y24_N3
\counter[28]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~83_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(28));

-- Location: LCCOMB_X41_Y24_N26
\Add1~84\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~84_combout\ = (counter(29) & (!\Add1~82\)) # (!counter(29) & ((\Add1~82\) # (GND)))
-- \Add1~85\ = CARRY((!\Add1~82\) # (!counter(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(29),
	datad => VCC,
	cin => \Add1~82\,
	combout => \Add1~84_combout\,
	cout => \Add1~85\);

-- Location: LCCOMB_X42_Y24_N28
\Add1~86\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~86_combout\ = (\counter[31]~1_combout\ & \Add1~84_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~84_combout\,
	combout => \Add1~86_combout\);

-- Location: LCFF_X42_Y24_N29
\counter[29]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~86_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(29));

-- Location: LCCOMB_X41_Y24_N30
\Add1~91\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~91_combout\ = \Add1~88\ $ (!counter(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => counter(31),
	cin => \Add1~88\,
	combout => \Add1~91_combout\);

-- Location: LCCOMB_X45_Y25_N12
\Add1~93\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~93_combout\ = (!\Add1~91_combout\) # (!\counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~91_combout\,
	combout => \Add1~93_combout\);

-- Location: LCFF_X45_Y25_N13
\counter[31]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~93_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(31));

-- Location: LCCOMB_X42_Y26_N28
\process_0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~2_combout\ = (!takt_counter(29) & !takt_counter(30))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(29),
	datad => takt_counter(30),
	combout => \process_0~2_combout\);

-- Location: LCCOMB_X44_Y26_N28
\process_0~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~9_combout\ = (!takt_counter(6) & (takt_counter(0) & (!takt_counter(4) & !takt_counter(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(6),
	datab => takt_counter(0),
	datac => takt_counter(4),
	datad => takt_counter(1),
	combout => \process_0~9_combout\);

-- Location: LCCOMB_X44_Y26_N14
\process_0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~10_combout\ = (!takt_counter(10) & (!takt_counter(12) & \process_0~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => takt_counter(10),
	datac => takt_counter(12),
	datad => \process_0~9_combout\,
	combout => \process_0~10_combout\);

-- Location: LCCOMB_X43_Y26_N20
\process_0~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~11_combout\ = (\process_0~8_combout\ & (\process_0~2_combout\ & (\process_0~10_combout\ & \process_0~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~8_combout\,
	datab => \process_0~2_combout\,
	datac => \process_0~10_combout\,
	datad => \process_0~7_combout\,
	combout => \process_0~11_combout\);

-- Location: LCCOMB_X43_Y26_N30
\LessThan1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan1~0_combout\ = (\Equal1~5_combout\ & (!counter(3) & (\Equal1~7_combout\ & \Equal1~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~5_combout\,
	datab => counter(3),
	datac => \Equal1~7_combout\,
	datad => \Equal1~4_combout\,
	combout => \LessThan1~0_combout\);

-- Location: LCCOMB_X43_Y26_N8
\process_0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~14_combout\ = (\process_0~13_combout\ & (\process_0~11_combout\ & ((\LessThan1~0_combout\) # (!counter(31)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~13_combout\,
	datab => counter(31),
	datac => \process_0~11_combout\,
	datad => \LessThan1~0_combout\,
	combout => \process_0~14_combout\);

-- Location: LCCOMB_X44_Y26_N16
\process_0~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~16_combout\ = (!takt_counter(2) & (\process_0~11_combout\ & \process_0~15_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => takt_counter(2),
	datac => \process_0~11_combout\,
	datad => \process_0~15_combout\,
	combout => \process_0~16_combout\);

-- Location: LCCOMB_X43_Y25_N18
\process_0~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \process_0~17_combout\ = (\process_0~16_combout\ & (((\Equal1~8_combout\ & \LessThan2~0_combout\)) # (!counter(31))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(31),
	datab => \Equal1~8_combout\,
	datac => \process_0~16_combout\,
	datad => \LessThan2~0_combout\,
	combout => \process_0~17_combout\);

-- Location: LCCOMB_X44_Y25_N20
\counter[31]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter[31]~1_combout\ = (\counter[31]~0_combout\ & ((\currentState.TRANSMIT~regout\ & ((\process_0~17_combout\))) # (!\currentState.TRANSMIT~regout\ & (\process_0~14_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datab => \counter[31]~0_combout\,
	datac => \process_0~14_combout\,
	datad => \process_0~17_combout\,
	combout => \counter[31]~1_combout\);

-- Location: LCCOMB_X41_Y25_N0
\Add1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~0_combout\ = counter(0) $ (GND)
-- \Add1~1\ = CARRY(!counter(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => counter(0),
	datad => VCC,
	combout => \Add1~0_combout\,
	cout => \Add1~1\);

-- Location: LCCOMB_X45_Y25_N8
\Add1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~2_combout\ = (!\Add1~0_combout\) # (!\counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter[31]~1_combout\,
	datad => \Add1~0_combout\,
	combout => \Add1~2_combout\);

-- Location: LCFF_X45_Y25_N9
\counter[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~2_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(0));

-- Location: LCCOMB_X41_Y25_N2
\Add1~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~3_combout\ = (counter(1) & (!\Add1~1\)) # (!counter(1) & ((\Add1~1\) # (GND)))
-- \Add1~4\ = CARRY((!\Add1~1\) # (!counter(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => counter(1),
	datad => VCC,
	cin => \Add1~1\,
	combout => \Add1~3_combout\,
	cout => \Add1~4\);

-- Location: LCCOMB_X45_Y25_N2
\Add1~95\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~95_combout\ = (\Add1~3_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~3_combout\,
	datac => \counter[31]~1_combout\,
	combout => \Add1~95_combout\);

-- Location: LCFF_X45_Y25_N3
\counter[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~95_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(1));

-- Location: LCCOMB_X45_Y25_N14
\Add1~94\ : cycloneii_lcell_comb
-- Equation(s):
-- \Add1~94_combout\ = (\Add1~5_combout\ & \counter[31]~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Add1~5_combout\,
	datac => \counter[31]~1_combout\,
	combout => \Add1~94_combout\);

-- Location: LCFF_X45_Y25_N15
\counter[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Add1~94_combout\,
	ena => \counter[31]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => counter(2));

-- Location: LCCOMB_X45_Y25_N0
\Equal1~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~9_combout\ = (counter(3) & (!counter(2) & counter(31)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(3),
	datac => counter(2),
	datad => counter(31),
	combout => \Equal1~9_combout\);

-- Location: LCCOMB_X43_Y25_N0
\Equal1~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal1~10_combout\ = (!counter(1) & (\Equal1~9_combout\ & (counter(0) & \Equal1~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => \Equal1~9_combout\,
	datac => counter(0),
	datad => \Equal1~8_combout\,
	combout => \Equal1~10_combout\);

-- Location: LCCOMB_X44_Y23_N22
\RxD_2_out~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_2_out~1_combout\ = (\RxD_2_out~0_combout\ & (\Equal1~10_combout\ & (\LessThan0~3_combout\ & !\process_0~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RxD_2_out~0_combout\,
	datab => \Equal1~10_combout\,
	datac => \LessThan0~3_combout\,
	datad => \process_0~14_combout\,
	combout => \RxD_2_out~1_combout\);

-- Location: LCFF_X44_Y23_N23
\currentState.DECODE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \currentState.DECODE~regout\);

-- Location: LCCOMB_X45_Y23_N2
\RxD_1_out~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_1_out~0_combout\ = (!\RST~combout\ & \currentState.DECODE~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RST~combout\,
	datac => \currentState.DECODE~regout\,
	combout => \RxD_1_out~0_combout\);

-- Location: LCFF_X45_Y23_N17
\RxD_1_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => \RxD~combout\,
	sload => VCC,
	ena => \RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_1_out~reg0_regout\);

-- Location: LCCOMB_X43_Y23_N20
\Selector1~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~1_combout\ = (counter(0) & counter(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => counter(0),
	datad => counter(2),
	combout => \Selector1~1_combout\);

-- Location: LCCOMB_X44_Y25_N8
\Selector1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~2_combout\ = (counter(1) & (\process_0~14_combout\ & \LessThan0~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datac => \process_0~14_combout\,
	datad => \LessThan0~3_combout\,
	combout => \Selector1~2_combout\);

-- Location: LCCOMB_X43_Y23_N30
\Selector1~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~3_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector1~2_combout\) # (!\Selector1~1_combout\)))) # (!\currentState.RECEIVE~regout\ & (\currentState.IDLE~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.IDLE~regout\,
	datab => \currentState.RECEIVE~regout\,
	datac => \Selector1~1_combout\,
	datad => \Selector1~2_combout\,
	combout => \Selector1~3_combout\);

-- Location: LCCOMB_X43_Y26_N26
\Selector3~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector3~1_combout\ = (\process_0~14_combout\ & \LessThan0~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \process_0~14_combout\,
	datad => \LessThan0~3_combout\,
	combout => \Selector3~1_combout\);

-- Location: LCCOMB_X43_Y23_N10
\Selector1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~0_combout\ = (\RxD~combout\ & (counter(1) & (\currentState.RECEIVE~regout\ & \Selector3~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RxD~combout\,
	datab => counter(1),
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector3~1_combout\,
	combout => \Selector1~0_combout\);

-- Location: LCCOMB_X43_Y23_N16
\Selector1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~4_combout\ = (\Selector1~1_combout\ & ((\Selector1~0_combout\) # ((\Selector1~3_combout\ & temp(6))))) # (!\Selector1~1_combout\ & (\Selector1~3_combout\ & (temp(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector1~1_combout\,
	datab => \Selector1~3_combout\,
	datac => temp(6),
	datad => \Selector1~0_combout\,
	combout => \Selector1~4_combout\);

-- Location: LCFF_X43_Y23_N17
\temp[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector1~4_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(6));

-- Location: LCCOMB_X44_Y23_N6
\RxD_2_out~reg0feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_2_out~reg0feeder_combout\ = temp(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => temp(6),
	combout => \RxD_2_out~reg0feeder_combout\);

-- Location: LCFF_X44_Y23_N7
\RxD_2_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \RxD_2_out~reg0feeder_combout\,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_2_out~reg0_regout\);

-- Location: LCCOMB_X43_Y26_N4
\Selector2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector2~2_combout\ = (\currentState.RECEIVE~regout\ & (!counter(0) & (\RxD~combout\ & \Selector3~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.RECEIVE~regout\,
	datab => counter(0),
	datac => \RxD~combout\,
	datad => \Selector3~1_combout\,
	combout => \Selector2~2_combout\);

-- Location: LCCOMB_X43_Y26_N0
\Selector2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector2~0_combout\ = (counter(1)) # ((counter(0)) # ((!\Selector3~1_combout\) # (!counter(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => counter(0),
	datac => counter(2),
	datad => \Selector3~1_combout\,
	combout => \Selector2~0_combout\);

-- Location: LCCOMB_X43_Y26_N2
\Selector2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector2~1_combout\ = (temp(5) & ((\currentState.RECEIVE~regout\ & ((\Selector2~0_combout\))) # (!\currentState.RECEIVE~regout\ & (\currentState.IDLE~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => temp(5),
	datab => \currentState.IDLE~regout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector2~0_combout\,
	combout => \Selector2~1_combout\);

-- Location: LCCOMB_X43_Y26_N16
\Selector2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector2~3_combout\ = (\Selector2~1_combout\) # ((!counter(1) & (counter(2) & \Selector2~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => counter(2),
	datac => \Selector2~2_combout\,
	datad => \Selector2~1_combout\,
	combout => \Selector2~3_combout\);

-- Location: LCFF_X43_Y26_N17
\temp[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector2~3_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(5));

-- Location: LCFF_X46_Y17_N17
\RxD_3_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(5),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_3_out~reg0_regout\);

-- Location: LCCOMB_X43_Y23_N8
\Selector1~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector1~5_combout\ = (\RxD~combout\ & (\currentState.RECEIVE~regout\ & (\LessThan0~3_combout\ & \process_0~14_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \RxD~combout\,
	datab => \currentState.RECEIVE~regout\,
	datac => \LessThan0~3_combout\,
	datad => \process_0~14_combout\,
	combout => \Selector1~5_combout\);

-- Location: LCCOMB_X43_Y23_N2
\Selector3~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector3~2_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector3~1_combout\)) # (!\Selector3~0_combout\))) # (!\currentState.RECEIVE~regout\ & (((\currentState.IDLE~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector3~0_combout\,
	datab => \currentState.IDLE~regout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector3~1_combout\,
	combout => \Selector3~2_combout\);

-- Location: LCCOMB_X43_Y23_N18
\Selector3~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector3~3_combout\ = (\Selector3~0_combout\ & ((\Selector1~5_combout\) # ((temp(4) & \Selector3~2_combout\)))) # (!\Selector3~0_combout\ & (((temp(4) & \Selector3~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector3~0_combout\,
	datab => \Selector1~5_combout\,
	datac => temp(4),
	datad => \Selector3~2_combout\,
	combout => \Selector3~3_combout\);

-- Location: LCFF_X43_Y23_N19
\temp[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector3~3_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(4));

-- Location: LCCOMB_X44_Y23_N0
\RxD_4_out~reg0feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_4_out~reg0feeder_combout\ = temp(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => temp(4),
	combout => \RxD_4_out~reg0feeder_combout\);

-- Location: LCFF_X44_Y23_N1
\RxD_4_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \RxD_4_out~reg0feeder_combout\,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_4_out~reg0_regout\);

-- Location: LCCOMB_X45_Y25_N30
\Selector4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector4~0_combout\ = (!counter(2) & !counter(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => counter(2),
	datac => counter(0),
	combout => \Selector4~0_combout\);

-- Location: LCCOMB_X43_Y23_N12
\Selector4~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector4~1_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector1~2_combout\)) # (!\Selector4~0_combout\))) # (!\currentState.RECEIVE~regout\ & (((\currentState.IDLE~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector4~0_combout\,
	datab => \currentState.IDLE~regout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector1~2_combout\,
	combout => \Selector4~1_combout\);

-- Location: LCCOMB_X44_Y23_N8
\Selector4~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector4~2_combout\ = (\Selector1~0_combout\ & ((\Selector4~0_combout\) # ((temp(3) & \Selector4~1_combout\)))) # (!\Selector1~0_combout\ & (((temp(3) & \Selector4~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector1~0_combout\,
	datab => \Selector4~0_combout\,
	datac => temp(3),
	datad => \Selector4~1_combout\,
	combout => \Selector4~2_combout\);

-- Location: LCFF_X44_Y23_N9
\temp[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector4~2_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(3));

-- Location: LCCOMB_X44_Y23_N18
\RxD_5_out~reg0feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_5_out~reg0feeder_combout\ = temp(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => temp(3),
	combout => \RxD_5_out~reg0feeder_combout\);

-- Location: LCFF_X44_Y23_N19
\RxD_5_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \RxD_5_out~reg0feeder_combout\,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_5_out~reg0_regout\);

-- Location: LCCOMB_X45_Y23_N24
\Selector5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector5~0_combout\ = (counter(0) & !counter(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => counter(0),
	datad => counter(2),
	combout => \Selector5~0_combout\);

-- Location: LCCOMB_X43_Y23_N14
\Selector5~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector5~1_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector1~2_combout\)) # (!\Selector5~0_combout\))) # (!\currentState.RECEIVE~regout\ & (((\currentState.IDLE~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector5~0_combout\,
	datab => \currentState.IDLE~regout\,
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector1~2_combout\,
	combout => \Selector5~1_combout\);

-- Location: LCCOMB_X44_Y23_N2
\Selector5~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector5~2_combout\ = (\Selector1~0_combout\ & ((\Selector5~0_combout\) # ((temp(2) & \Selector5~1_combout\)))) # (!\Selector1~0_combout\ & (((temp(2) & \Selector5~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector1~0_combout\,
	datab => \Selector5~0_combout\,
	datac => temp(2),
	datad => \Selector5~1_combout\,
	combout => \Selector5~2_combout\);

-- Location: LCFF_X44_Y23_N3
\temp[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector5~2_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(2));

-- Location: LCCOMB_X44_Y23_N12
\RxD_6_out~reg0feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \RxD_6_out~reg0feeder_combout\ = temp(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => temp(2),
	combout => \RxD_6_out~reg0feeder_combout\);

-- Location: LCFF_X44_Y23_N13
\RxD_6_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \RxD_6_out~reg0feeder_combout\,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_6_out~reg0_regout\);

-- Location: LCCOMB_X43_Y23_N0
\Selector6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector6~0_combout\ = (counter(1)) # ((counter(2)) # ((counter(0)) # (!\Selector3~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => counter(2),
	datac => counter(0),
	datad => \Selector3~1_combout\,
	combout => \Selector6~0_combout\);

-- Location: LCCOMB_X43_Y23_N26
\Selector6~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector6~1_combout\ = (temp(1) & ((\currentState.RECEIVE~regout\ & ((\Selector6~0_combout\))) # (!\currentState.RECEIVE~regout\ & (\currentState.IDLE~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.IDLE~regout\,
	datab => temp(1),
	datac => \currentState.RECEIVE~regout\,
	datad => \Selector6~0_combout\,
	combout => \Selector6~1_combout\);

-- Location: LCCOMB_X43_Y23_N28
\Selector6~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector6~2_combout\ = (\Selector6~1_combout\) # ((!counter(1) & (!counter(2) & \Selector2~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(1),
	datab => counter(2),
	datac => \Selector2~2_combout\,
	datad => \Selector6~1_combout\,
	combout => \Selector6~2_combout\);

-- Location: LCFF_X43_Y23_N29
\temp[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector6~2_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(1));

-- Location: LCFF_X46_Y17_N11
\RxD_7_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(1),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_7_out~reg0_regout\);

-- Location: LCCOMB_X43_Y23_N4
\Selector7~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector7~0_combout\ = (!counter(1) & (counter(0) & !counter(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => counter(1),
	datac => counter(0),
	datad => counter(2),
	combout => \Selector7~0_combout\);

-- Location: LCCOMB_X43_Y23_N22
\Selector7~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector7~1_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector3~1_combout\) # (!\Selector7~0_combout\)))) # (!\currentState.RECEIVE~regout\ & (\currentState.IDLE~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.IDLE~regout\,
	datab => \currentState.RECEIVE~regout\,
	datac => \Selector7~0_combout\,
	datad => \Selector3~1_combout\,
	combout => \Selector7~1_combout\);

-- Location: LCCOMB_X43_Y23_N6
\Selector7~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector7~2_combout\ = (\Selector7~0_combout\ & ((\Selector1~5_combout\) # ((temp(0) & \Selector7~1_combout\)))) # (!\Selector7~0_combout\ & (((temp(0) & \Selector7~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector7~0_combout\,
	datab => \Selector1~5_combout\,
	datac => temp(0),
	datad => \Selector7~1_combout\,
	combout => \Selector7~2_combout\);

-- Location: LCFF_X43_Y23_N7
\temp[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector7~2_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(0));

-- Location: LCFF_X46_Y17_N13
\RxD_8_out~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(0),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \RxD_8_out~reg0_regout\);

-- Location: LCCOMB_X45_Y26_N8
\Selector40~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector40~0_combout\ = (\currentState.TRANSMIT~regout\) # ((\BusyTxD~reg0_regout\ & \currentState.IDLE~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datac => \BusyTxD~reg0_regout\,
	datad => \currentState.IDLE~regout\,
	combout => \Selector40~0_combout\);

-- Location: LCFF_X45_Y26_N9
\BusyTxD~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector40~0_combout\,
	ena => \ALT_INV_RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \BusyTxD~reg0_regout\);

-- Location: LCFF_X46_Y17_N1
\Parallel1[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(1),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(1));

-- Location: LCFF_X45_Y17_N31
\Parallel1[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(2),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(2));

-- Location: LCFF_X45_Y17_N1
\Parallel1[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(3),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(3));

-- Location: LCFF_X45_Y17_N19
\Parallel1[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(6),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(6));

-- Location: LCCOMB_X45_Y17_N18
\rm_dec|maj3|Add3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj3|Add3~0_combout\ = (Parallel1(7) & (Parallel1(3) & (Parallel1(6) $ (!Parallel1(2))))) # (!Parallel1(7) & (!Parallel1(3) & (Parallel1(6) $ (!Parallel1(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(7),
	datab => Parallel1(3),
	datac => Parallel1(6),
	datad => Parallel1(2),
	combout => \rm_dec|maj3|Add3~0_combout\);

-- Location: LCFF_X46_Y17_N27
\Parallel1[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(5),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(5));

-- Location: LCCOMB_X46_Y17_N14
\rm_dec|maj3|scalar~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj3|scalar~1_combout\ = Parallel1(1) $ (Parallel1(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(1),
	datad => Parallel1(5),
	combout => \rm_dec|maj3|scalar~1_combout\);

-- Location: LCCOMB_X45_Y17_N0
\rm_dec|maj3|Add2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj3|Add2~0_combout\ = (Parallel1(7) & (!Parallel1(3) & (Parallel1(6) $ (Parallel1(2))))) # (!Parallel1(7) & (Parallel1(3) & (Parallel1(6) $ (Parallel1(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(7),
	datab => Parallel1(6),
	datac => Parallel1(3),
	datad => Parallel1(2),
	combout => \rm_dec|maj3|Add2~0_combout\);

-- Location: LCCOMB_X45_Y17_N12
\rm_dec|maj3|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj3|LessThan0~0_combout\ = (\rm_dec|maj3|scalar~0_combout\ & (((\rm_dec|maj3|scalar~1_combout\) # (\rm_dec|maj3|Add2~0_combout\)) # (!\rm_dec|maj3|Add3~0_combout\))) # (!\rm_dec|maj3|scalar~0_combout\ & ((\rm_dec|maj3|Add3~0_combout\ & 
-- (\rm_dec|maj3|scalar~1_combout\ & \rm_dec|maj3|Add2~0_combout\)) # (!\rm_dec|maj3|Add3~0_combout\ & ((\rm_dec|maj3|scalar~1_combout\) # (\rm_dec|maj3|Add2~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj3|scalar~0_combout\,
	datab => \rm_dec|maj3|Add3~0_combout\,
	datac => \rm_dec|maj3|scalar~1_combout\,
	datad => \rm_dec|maj3|Add2~0_combout\,
	combout => \rm_dec|maj3|LessThan0~0_combout\);

-- Location: LCCOMB_X46_Y17_N30
\rm_dec|last_maj|Add2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add2~3_combout\ = Parallel1(0) $ (Parallel1(1) $ (Parallel1(2) $ (!\rm_dec|maj3|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(0),
	datab => Parallel1(1),
	datac => Parallel1(2),
	datad => \rm_dec|maj3|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|Add2~3_combout\);

-- Location: LCCOMB_X45_Y23_N18
\Selector0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector0~0_combout\ = (!counter(0) & counter(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => counter(0),
	datad => counter(2),
	combout => \Selector0~0_combout\);

-- Location: LCCOMB_X45_Y23_N28
\Selector0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector0~1_combout\ = (\currentState.RECEIVE~regout\ & (((!\Selector0~0_combout\) # (!\Selector1~2_combout\)))) # (!\currentState.RECEIVE~regout\ & (\currentState.IDLE~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010111011101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.IDLE~regout\,
	datab => \currentState.RECEIVE~regout\,
	datac => \Selector1~2_combout\,
	datad => \Selector0~0_combout\,
	combout => \Selector0~1_combout\);

-- Location: LCCOMB_X44_Y23_N4
\Selector0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector0~2_combout\ = (\Selector1~0_combout\ & ((\Selector0~0_combout\) # ((temp(7) & \Selector0~1_combout\)))) # (!\Selector1~0_combout\ & (((temp(7) & \Selector0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Selector1~0_combout\,
	datab => \Selector0~0_combout\,
	datac => temp(7),
	datad => \Selector0~1_combout\,
	combout => \Selector0~2_combout\);

-- Location: LCFF_X44_Y23_N5
\temp[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector0~2_combout\,
	sclr => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => temp(7));

-- Location: LCFF_X45_Y17_N29
\Parallel1[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(7),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(7));

-- Location: LCFF_X45_Y17_N15
\Parallel1[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(4),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(4));

-- Location: LCCOMB_X45_Y17_N28
\rm_dec|maj2|Add3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj2|Add3~0_combout\ = (Parallel1(5) & (Parallel1(7) & (Parallel1(6) $ (!Parallel1(4))))) # (!Parallel1(5) & (!Parallel1(7) & (Parallel1(6) $ (!Parallel1(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(5),
	datab => Parallel1(6),
	datac => Parallel1(7),
	datad => Parallel1(4),
	combout => \rm_dec|maj2|Add3~0_combout\);

-- Location: LCCOMB_X46_Y17_N10
\rm_dec|maj2|scalar~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj2|scalar~1_combout\ = Parallel1(1) $ (Parallel1(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(1),
	datad => Parallel1(3),
	combout => \rm_dec|maj2|scalar~1_combout\);

-- Location: LCFF_X46_Y17_N7
\Parallel1[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	sdata => temp(0),
	sload => VCC,
	ena => \RxD_2_out~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Parallel1(0));

-- Location: LCCOMB_X46_Y17_N20
\rm_dec|maj2|scalar~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj2|scalar~0_combout\ = Parallel1(2) $ (Parallel1(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => Parallel1(2),
	datad => Parallel1(0),
	combout => \rm_dec|maj2|scalar~0_combout\);

-- Location: LCCOMB_X45_Y17_N2
\rm_dec|maj2|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj2|LessThan0~0_combout\ = (\rm_dec|maj2|Add2~0_combout\ & (((\rm_dec|maj2|scalar~1_combout\) # (\rm_dec|maj2|scalar~0_combout\)) # (!\rm_dec|maj2|Add3~0_combout\))) # (!\rm_dec|maj2|Add2~0_combout\ & ((\rm_dec|maj2|Add3~0_combout\ & 
-- (\rm_dec|maj2|scalar~1_combout\ & \rm_dec|maj2|scalar~0_combout\)) # (!\rm_dec|maj2|Add3~0_combout\ & ((\rm_dec|maj2|scalar~1_combout\) # (\rm_dec|maj2|scalar~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101110110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj2|Add2~0_combout\,
	datab => \rm_dec|maj2|Add3~0_combout\,
	datac => \rm_dec|maj2|scalar~1_combout\,
	datad => \rm_dec|maj2|scalar~0_combout\,
	combout => \rm_dec|maj2|LessThan0~0_combout\);

-- Location: LCCOMB_X45_Y17_N14
\rm_dec|maj1|scalar~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj1|scalar~2_combout\ = Parallel1(7) $ (Parallel1(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(7),
	datad => Parallel1(6),
	combout => \rm_dec|maj1|scalar~2_combout\);

-- Location: LCCOMB_X46_Y17_N12
\rm_dec|maj1|scalar~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj1|scalar~1_combout\ = Parallel1(1) $ (Parallel1(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(1),
	datad => Parallel1(0),
	combout => \rm_dec|maj1|scalar~1_combout\);

-- Location: LCCOMB_X45_Y17_N30
\rm_dec|maj1|scalar~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj1|scalar~0_combout\ = Parallel1(2) $ (Parallel1(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => Parallel1(2),
	datad => Parallel1(3),
	combout => \rm_dec|maj1|scalar~0_combout\);

-- Location: LCCOMB_X45_Y17_N16
\rm_dec|maj1|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|maj1|LessThan0~0_combout\ = (\rm_dec|maj1|scalar~3_combout\ & ((\rm_dec|maj1|scalar~2_combout\) # ((\rm_dec|maj1|scalar~1_combout\) # (\rm_dec|maj1|scalar~0_combout\)))) # (!\rm_dec|maj1|scalar~3_combout\ & ((\rm_dec|maj1|scalar~2_combout\ & 
-- ((\rm_dec|maj1|scalar~1_combout\) # (\rm_dec|maj1|scalar~0_combout\))) # (!\rm_dec|maj1|scalar~2_combout\ & (\rm_dec|maj1|scalar~1_combout\ & \rm_dec|maj1|scalar~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj1|scalar~3_combout\,
	datab => \rm_dec|maj1|scalar~2_combout\,
	datac => \rm_dec|maj1|scalar~1_combout\,
	datad => \rm_dec|maj1|scalar~0_combout\,
	combout => \rm_dec|maj1|LessThan0~0_combout\);

-- Location: LCCOMB_X45_Y17_N8
\rm_dec|last_maj|vec_cobined~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~2_combout\ = Parallel1(0) $ (\rm_dec|maj2|LessThan0~0_combout\ $ (\rm_dec|maj1|LessThan0~0_combout\ $ (!\rm_dec|maj3|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => Parallel1(0),
	datab => \rm_dec|maj2|LessThan0~0_combout\,
	datac => \rm_dec|maj1|LessThan0~0_combout\,
	datad => \rm_dec|maj3|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~2_combout\);

-- Location: LCCOMB_X45_Y17_N20
\rm_dec|last_maj|vec_cobined~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~0_combout\ = \rm_dec|maj3|LessThan0~0_combout\ $ (Parallel1(1) $ (\rm_dec|maj2|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj3|LessThan0~0_combout\,
	datac => Parallel1(1),
	datad => \rm_dec|maj2|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~0_combout\);

-- Location: LCCOMB_X45_Y17_N22
\rm_dec|last_maj|vec_cobined~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~1_combout\ = Parallel1(2) $ (\rm_dec|maj1|LessThan0~0_combout\ $ (\rm_dec|maj3|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(2),
	datac => \rm_dec|maj1|LessThan0~0_combout\,
	datad => \rm_dec|maj3|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~1_combout\);

-- Location: LCCOMB_X45_Y17_N6
\rm_dec|last_maj|Add2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add2~2_combout\ = (\rm_dec|last_maj|ones~0_combout\ & ((\rm_dec|last_maj|vec_cobined~2_combout\) # ((!\rm_dec|last_maj|vec_cobined~0_combout\)))) # (!\rm_dec|last_maj|ones~0_combout\ & (\rm_dec|last_maj|vec_cobined~1_combout\ $ 
-- (((\rm_dec|last_maj|vec_cobined~2_combout\) # (!\rm_dec|last_maj|vec_cobined~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001101011001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|ones~0_combout\,
	datab => \rm_dec|last_maj|vec_cobined~2_combout\,
	datac => \rm_dec|last_maj|vec_cobined~0_combout\,
	datad => \rm_dec|last_maj|vec_cobined~1_combout\,
	combout => \rm_dec|last_maj|Add2~2_combout\);

-- Location: LCCOMB_X45_Y17_N4
\rm_dec|last_maj|vec_cobined~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~4_combout\ = Parallel1(3) $ (!\rm_dec|maj3|LessThan0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => Parallel1(3),
	datad => \rm_dec|maj3|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~4_combout\);

-- Location: LCCOMB_X45_Y17_N26
\rm_dec|last_maj|vec_cobined~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~3_combout\ = Parallel1(4) $ (\rm_dec|maj1|LessThan0~0_combout\ $ (\rm_dec|maj2|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(4),
	datac => \rm_dec|maj1|LessThan0~0_combout\,
	datad => \rm_dec|maj2|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~3_combout\);

-- Location: LCCOMB_X46_Y17_N18
\rm_dec|last_maj|Add6~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add6~1_combout\ = (\rm_dec|last_maj|vec_cobined~4_combout\ & ((\rm_dec|maj2|LessThan0~0_combout\ $ (!Parallel1(5))) # (!\rm_dec|last_maj|vec_cobined~3_combout\))) # (!\rm_dec|last_maj|vec_cobined~4_combout\ & 
-- (!\rm_dec|last_maj|vec_cobined~3_combout\ & (\rm_dec|maj2|LessThan0~0_combout\ $ (!Parallel1(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000011111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj2|LessThan0~0_combout\,
	datab => Parallel1(5),
	datac => \rm_dec|last_maj|vec_cobined~4_combout\,
	datad => \rm_dec|last_maj|vec_cobined~3_combout\,
	combout => \rm_dec|last_maj|Add6~1_combout\);

-- Location: LCCOMB_X46_Y17_N28
\rm_dec|last_maj|Add8~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add8~1_combout\ = (\rm_dec|last_maj|Add2~2_combout\ & (\rm_dec|last_maj|Add6~0_combout\ & (!\rm_dec|last_maj|Add2~3_combout\ & !\rm_dec|last_maj|Add6~1_combout\))) # (!\rm_dec|last_maj|Add2~2_combout\ & (((\rm_dec|last_maj|Add6~0_combout\ 
-- & !\rm_dec|last_maj|Add2~3_combout\)) # (!\rm_dec|last_maj|Add6~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add6~0_combout\,
	datab => \rm_dec|last_maj|Add2~3_combout\,
	datac => \rm_dec|last_maj|Add2~2_combout\,
	datad => \rm_dec|last_maj|Add6~1_combout\,
	combout => \rm_dec|last_maj|Add8~1_combout\);

-- Location: LCCOMB_X46_Y17_N24
\rm_dec|last_maj|Add6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add6~0_combout\ = \rm_dec|maj1|scalar~3_combout\ $ (\rm_dec|maj1|LessThan0~0_combout\ $ (Parallel1(3) $ (\rm_dec|maj3|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj1|scalar~3_combout\,
	datab => \rm_dec|maj1|LessThan0~0_combout\,
	datac => Parallel1(3),
	datad => \rm_dec|maj3|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|Add6~0_combout\);

-- Location: LCCOMB_X47_Y17_N26
\rm_dec|last_maj|Add8~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add8~2_combout\ = \rm_dec|last_maj|Add2~2_combout\ $ (\rm_dec|last_maj|Add6~1_combout\ $ (((!\rm_dec|last_maj|Add2~3_combout\ & \rm_dec|last_maj|Add6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101110110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add2~3_combout\,
	datab => \rm_dec|last_maj|Add6~0_combout\,
	datac => \rm_dec|last_maj|Add2~2_combout\,
	datad => \rm_dec|last_maj|Add6~1_combout\,
	combout => \rm_dec|last_maj|Add8~2_combout\);

-- Location: LCCOMB_X48_Y17_N0
\rm_dec|last_maj|vec_cobined~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|vec_cobined~5_combout\ = Parallel1(6) $ (!\rm_dec|maj1|LessThan0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => Parallel1(6),
	datad => \rm_dec|maj1|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|vec_cobined~5_combout\);

-- Location: LCCOMB_X47_Y17_N0
\rm_dec|last_maj|Add10~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add10~1_cout\ = CARRY(Parallel1(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(7),
	datad => VCC,
	cout => \rm_dec|last_maj|Add10~1_cout\);

-- Location: LCCOMB_X47_Y17_N2
\rm_dec|last_maj|Add10~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add10~2_combout\ = (\rm_dec|last_maj|Add8~0_combout\ & ((\rm_dec|last_maj|vec_cobined~5_combout\ & ((\rm_dec|last_maj|Add10~1_cout\) # (GND))) # (!\rm_dec|last_maj|vec_cobined~5_combout\ & (!\rm_dec|last_maj|Add10~1_cout\)))) # 
-- (!\rm_dec|last_maj|Add8~0_combout\ & ((\rm_dec|last_maj|vec_cobined~5_combout\ & (!\rm_dec|last_maj|Add10~1_cout\)) # (!\rm_dec|last_maj|vec_cobined~5_combout\ & (\rm_dec|last_maj|Add10~1_cout\ & VCC))))
-- \rm_dec|last_maj|Add10~3\ = CARRY((\rm_dec|last_maj|Add8~0_combout\ & ((\rm_dec|last_maj|vec_cobined~5_combout\) # (!\rm_dec|last_maj|Add10~1_cout\))) # (!\rm_dec|last_maj|Add8~0_combout\ & (\rm_dec|last_maj|vec_cobined~5_combout\ & 
-- !\rm_dec|last_maj|Add10~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011010001110",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add8~0_combout\,
	datab => \rm_dec|last_maj|vec_cobined~5_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add10~1_cout\,
	combout => \rm_dec|last_maj|Add10~2_combout\,
	cout => \rm_dec|last_maj|Add10~3\);

-- Location: LCCOMB_X47_Y17_N4
\rm_dec|last_maj|Add10~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add10~4_combout\ = (\rm_dec|last_maj|Add8~2_combout\ & (\rm_dec|last_maj|Add10~3\ $ (GND))) # (!\rm_dec|last_maj|Add8~2_combout\ & (!\rm_dec|last_maj|Add10~3\ & VCC))
-- \rm_dec|last_maj|Add10~5\ = CARRY((\rm_dec|last_maj|Add8~2_combout\ & !\rm_dec|last_maj|Add10~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \rm_dec|last_maj|Add8~2_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add10~3\,
	combout => \rm_dec|last_maj|Add10~4_combout\,
	cout => \rm_dec|last_maj|Add10~5\);

-- Location: LCCOMB_X47_Y17_N8
\rm_dec|last_maj|Add10~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add10~8_combout\ = !\rm_dec|last_maj|Add10~7\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	cin => \rm_dec|last_maj|Add10~7\,
	combout => \rm_dec|last_maj|Add10~8_combout\);

-- Location: LCCOMB_X46_Y17_N4
\rm_dec|last_maj|Add7~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add7~0_combout\ = (\rm_dec|last_maj|vec_cobined~4_combout\ & (\rm_dec|last_maj|vec_cobined~3_combout\ & (\rm_dec|maj2|LessThan0~0_combout\ $ (Parallel1(5))))) # (!\rm_dec|last_maj|vec_cobined~4_combout\ & 
-- ((\rm_dec|last_maj|vec_cobined~3_combout\) # (\rm_dec|maj2|LessThan0~0_combout\ $ (Parallel1(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110111100000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|maj2|LessThan0~0_combout\,
	datab => Parallel1(5),
	datac => \rm_dec|last_maj|vec_cobined~4_combout\,
	datad => \rm_dec|last_maj|vec_cobined~3_combout\,
	combout => \rm_dec|last_maj|Add7~0_combout\);

-- Location: LCCOMB_X47_Y17_N30
\rm_dec|last_maj|Add3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add3~0_combout\ = (\rm_dec|last_maj|ones~0_combout\ & ((\rm_dec|last_maj|vec_cobined~0_combout\) # ((!\rm_dec|last_maj|vec_cobined~2_combout\)))) # (!\rm_dec|last_maj|ones~0_combout\ & (\rm_dec|last_maj|vec_cobined~1_combout\ $ 
-- (((!\rm_dec|last_maj|vec_cobined~0_combout\ & \rm_dec|last_maj|vec_cobined~2_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100100111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|ones~0_combout\,
	datab => \rm_dec|last_maj|vec_cobined~0_combout\,
	datac => \rm_dec|last_maj|vec_cobined~1_combout\,
	datad => \rm_dec|last_maj|vec_cobined~2_combout\,
	combout => \rm_dec|last_maj|Add3~0_combout\);

-- Location: LCCOMB_X46_Y17_N22
\rm_dec|last_maj|Add9~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add9~0_combout\ = (\rm_dec|last_maj|Add7~0_combout\ & (!\rm_dec|last_maj|Add6~0_combout\ & (\rm_dec|last_maj|Add2~3_combout\ & !\rm_dec|last_maj|Add3~0_combout\))) # (!\rm_dec|last_maj|Add7~0_combout\ & 
-- (((!\rm_dec|last_maj|Add6~0_combout\ & \rm_dec|last_maj|Add2~3_combout\)) # (!\rm_dec|last_maj|Add3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010001001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add6~0_combout\,
	datab => \rm_dec|last_maj|Add2~3_combout\,
	datac => \rm_dec|last_maj|Add7~0_combout\,
	datad => \rm_dec|last_maj|Add3~0_combout\,
	combout => \rm_dec|last_maj|Add9~0_combout\);

-- Location: LCCOMB_X47_Y17_N12
\rm_dec|last_maj|Add11~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add11~1_cout\ = CARRY(!Parallel1(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => Parallel1(7),
	datad => VCC,
	cout => \rm_dec|last_maj|Add11~1_cout\);

-- Location: LCCOMB_X47_Y17_N14
\rm_dec|last_maj|Add11~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|Add11~2_combout\ = (\rm_dec|last_maj|Add8~0_combout\ & ((\rm_dec|last_maj|vec_cobined~5_combout\ & (!\rm_dec|last_maj|Add11~1_cout\)) # (!\rm_dec|last_maj|vec_cobined~5_combout\ & ((\rm_dec|last_maj|Add11~1_cout\) # (GND))))) # 
-- (!\rm_dec|last_maj|Add8~0_combout\ & ((\rm_dec|last_maj|vec_cobined~5_combout\ & (\rm_dec|last_maj|Add11~1_cout\ & VCC)) # (!\rm_dec|last_maj|vec_cobined~5_combout\ & (!\rm_dec|last_maj|Add11~1_cout\))))
-- \rm_dec|last_maj|Add11~3\ = CARRY((\rm_dec|last_maj|Add8~0_combout\ & ((!\rm_dec|last_maj|Add11~1_cout\) # (!\rm_dec|last_maj|vec_cobined~5_combout\))) # (!\rm_dec|last_maj|Add8~0_combout\ & (!\rm_dec|last_maj|vec_cobined~5_combout\ & 
-- !\rm_dec|last_maj|Add11~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add8~0_combout\,
	datab => \rm_dec|last_maj|vec_cobined~5_combout\,
	datad => VCC,
	cin => \rm_dec|last_maj|Add11~1_cout\,
	combout => \rm_dec|last_maj|Add11~2_combout\,
	cout => \rm_dec|last_maj|Add11~3\);

-- Location: LCCOMB_X47_Y17_N28
\rm_dec|last_maj|LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|LessThan0~0_combout\ = (\rm_dec|last_maj|Add11~4_combout\ & (((\rm_dec|last_maj|Add11~2_combout\ & !\rm_dec|last_maj|Add10~2_combout\)) # (!\rm_dec|last_maj|Add10~4_combout\))) # (!\rm_dec|last_maj|Add11~4_combout\ & 
-- (\rm_dec|last_maj|Add11~2_combout\ & (!\rm_dec|last_maj|Add10~4_combout\ & !\rm_dec|last_maj|Add10~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add11~4_combout\,
	datab => \rm_dec|last_maj|Add11~2_combout\,
	datac => \rm_dec|last_maj|Add10~4_combout\,
	datad => \rm_dec|last_maj|Add10~2_combout\,
	combout => \rm_dec|last_maj|LessThan0~0_combout\);

-- Location: LCCOMB_X47_Y17_N22
\rm_dec|last_maj|LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|LessThan0~1_combout\ = (\rm_dec|last_maj|Add10~6_combout\ & (\rm_dec|last_maj|Add11~6_combout\ & \rm_dec|last_maj|LessThan0~0_combout\)) # (!\rm_dec|last_maj|Add10~6_combout\ & ((\rm_dec|last_maj|Add11~6_combout\) # 
-- (\rm_dec|last_maj|LessThan0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110101000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add10~6_combout\,
	datab => \rm_dec|last_maj|Add11~6_combout\,
	datad => \rm_dec|last_maj|LessThan0~0_combout\,
	combout => \rm_dec|last_maj|LessThan0~1_combout\);

-- Location: LCCOMB_X47_Y17_N10
\rm_dec|last_maj|LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \rm_dec|last_maj|LessThan0~2_combout\ = (\rm_dec|last_maj|Add11~8_combout\ & (\rm_dec|last_maj|Add10~8_combout\ & !\rm_dec|last_maj|LessThan0~1_combout\)) # (!\rm_dec|last_maj|Add11~8_combout\ & ((\rm_dec|last_maj|Add10~8_combout\) # 
-- (!\rm_dec|last_maj|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rm_dec|last_maj|Add11~8_combout\,
	datac => \rm_dec|last_maj|Add10~8_combout\,
	datad => \rm_dec|last_maj|LessThan0~1_combout\,
	combout => \rm_dec|last_maj|LessThan0~2_combout\);

-- Location: LCFF_X47_Y17_N11
\dataBuffer[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \rm_dec|last_maj|LessThan0~2_combout\,
	ena => \RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dataBuffer(4));

-- Location: LCFF_X45_Y17_N17
\dataBuffer[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \rm_dec|maj1|LessThan0~0_combout\,
	ena => \RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dataBuffer(1));

-- Location: LCCOMB_X45_Y25_N20
\Selector41~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~0_combout\ = (!counter(1) & (!counter(0) & ((counter(3)) # (dataBuffer(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(3),
	datab => counter(1),
	datac => counter(0),
	datad => dataBuffer(1),
	combout => \Selector41~0_combout\);

-- Location: LCFF_X45_Y17_N13
\dataBuffer[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \rm_dec|maj3|LessThan0~0_combout\,
	ena => \RxD_1_out~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dataBuffer(3));

-- Location: LCCOMB_X45_Y25_N6
\Selector41~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~1_combout\ = (counter(1) & ((counter(0) & (dataBuffer(2))) # (!counter(0) & ((dataBuffer(3))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => dataBuffer(2),
	datab => counter(0),
	datac => dataBuffer(3),
	datad => counter(1),
	combout => \Selector41~1_combout\);

-- Location: LCCOMB_X45_Y25_N24
\Selector41~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~2_combout\ = (!counter(2) & ((\Selector41~0_combout\) # ((!counter(3) & \Selector41~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(3),
	datab => counter(2),
	datac => \Selector41~0_combout\,
	datad => \Selector41~1_combout\,
	combout => \Selector41~2_combout\);

-- Location: LCCOMB_X43_Y23_N24
\Selector3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector3~0_combout\ = (!counter(1) & (counter(0) & counter(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => counter(1),
	datac => counter(0),
	datad => counter(2),
	combout => \Selector3~0_combout\);

-- Location: LCCOMB_X45_Y25_N18
\Selector41~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~3_combout\ = (\Selector41~2_combout\) # ((!counter(3) & (dataBuffer(4) & \Selector3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => counter(3),
	datab => dataBuffer(4),
	datac => \Selector41~2_combout\,
	datad => \Selector3~0_combout\,
	combout => \Selector41~3_combout\);

-- Location: LCCOMB_X44_Y25_N22
\Selector41~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~4_combout\ = (\LessThan0~3_combout\ & ((\process_0~17_combout\ & ((\Selector41~3_combout\))) # (!\process_0~17_combout\ & (\DataTxD~reg0_regout\)))) # (!\LessThan0~3_combout\ & (\DataTxD~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \DataTxD~reg0_regout\,
	datab => \LessThan0~3_combout\,
	datac => \Selector41~3_combout\,
	datad => \process_0~17_combout\,
	combout => \Selector41~4_combout\);

-- Location: LCCOMB_X44_Y25_N6
\Selector41~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Selector41~5_combout\ = (\currentState.TRANSMIT~regout\ & (((\Selector41~4_combout\)))) # (!\currentState.TRANSMIT~regout\ & (((\DataTxD~reg0_regout\)) # (!\currentState.IDLE~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101101010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \currentState.TRANSMIT~regout\,
	datab => \currentState.IDLE~regout\,
	datac => \DataTxD~reg0_regout\,
	datad => \Selector41~4_combout\,
	combout => \Selector41~5_combout\);

-- Location: LCFF_X44_Y25_N7
\DataTxD~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLK~clkctrl_outclk\,
	datain => \Selector41~5_combout\,
	ena => \ALT_INV_RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \DataTxD~reg0_regout\);

-- Location: PIN_AD12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RST_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RST~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RST_out);

-- Location: PIN_AE13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_1_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_1_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_1_out);

-- Location: PIN_AF13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_2_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_2_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_2_out);

-- Location: PIN_AE15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_3_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_3_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_3_out);

-- Location: PIN_AD15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_4_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_4_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_4_out);

-- Location: PIN_AC14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_5_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_5_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_5_out);

-- Location: PIN_AA13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_6_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_6_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_6_out);

-- Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_7_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_7_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_7_out);

-- Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RxD_8_out~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \RxD_8_out~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RxD_8_out);

-- Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\BusyTxD~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \BusyTxD~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_BusyTxD);

-- Location: PIN_B25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DataTxD~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \DataTxD~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_DataTxD);
END structure;


