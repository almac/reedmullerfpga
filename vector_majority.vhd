library IEEE; 
use IEEE.std_logic_1164.all;  
use ieee.std_logic_arith.all ;
use ieee.std_logic_unsigned.all;

entity vector_majority is 
	port (  
		me: in std_logic_vector(7 downto 0) ;  
		x1: in std_logic_vector(7 downto 0) ;
		x2: in std_logic_vector(7 downto 0) ;
		start: in std_logic;
		data_ready: out std_logic;
		majority: out std_logic 
	);  
end vector_majority;  

architecture rtl of vector_majority is  
	
	--variable notX1, notX2, adder1,adder2,adder3, adder4: std_logic_vector(7 downto 0);
	--variable bit_counter: std_logic_vector (3 downto 0);
	
	function get_scalar (x,y: std_logic_vector) return std_logic is

		variable scalar: std_logic;
		begin
		scalar :=x(0) and y(0);
		for i in 1 to 7 loop
			scalar :=scalar xor (x(i) and y(i));
		end loop;
		return scalar;
	
	end function;
	
	function get_majority (x: std_logic_vector) return std_logic is

		variable zeros : integer := 0;
		variable ones : integer := 0;

		begin
		for i in 0 to 3 loop
			if (x(i) = '1') then
				ones:=ones+1;
			else
				zeros:=zeros+1;
			end if;
		end loop;
	
		if zeros>ones then
			return '0';
		else
			return '1';
		end if;
	end function;
	
begin
	process(start,me)
	
	variable notX1, notX2, adder1,adder2,adder3, adder4: std_logic_vector(7 downto 0);
	variable bit_counter: std_logic_vector (3 downto 0);
	
		begin
		if (start = '1') then
			notX1 := not(x1);
			notX2 := not(x2);
	
			adder1 := x1 and x2;
			adder2 := x1 and notX2;
			adder3 := notX1 and x2;
			adder4 := notX1 and notX2;
	
			bit_counter(0) := get_scalar(adder1,me);
			bit_counter(1) := get_scalar(adder2,me);
			bit_counter(2) := get_scalar(adder3,me);
			bit_counter(3) := get_scalar(adder4,me);
	
			data_ready <= '1';
			majority <= get_majority(bit_counter);
		end if;
	end process;

end rtl;  